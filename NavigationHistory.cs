﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortableRazor.Utilities;

namespace PortableRazor
{
    /// <summary>
    /// Abstract element in the navigation history.
    /// Each item represents a view in some way, depending on the paradigm (MVC or MVVM)
    /// In case of navigation slots, multiple items can be visible at the same time.
    /// </summary>
    public abstract class NavigationHistoryItem : IDisposable
    {
        /// <summary>
        /// Is called when the item get dropped.
        /// Usually, this item is visible to that point OR the history got cleared.
        /// </summary>
        public abstract void Dispose();

        /// <summary>
        /// Is called when this item got the top most again (after going back from another one)
        /// Calling "back" another time, this item gets dropped.
        /// </summary>
        /// <returns>returns true if navigating back was successful</returns>
        protected abstract Task<bool> NavigatedBackAsync();


        internal async Task<bool> CallNavigateBackAsync()
        {
            return await NavigatedBackAsync();
        }
    }

    public class NavigationHistory
    {
        private readonly PortableRazorContext m_Context;
        private readonly Stack<NavigationHistoryItem> m_History = new Stack<NavigationHistoryItem>();
        protected readonly Logging Log;

        /// <summary>
        /// Creates a new History-Stack.
        /// Usually this only done by the PortableRazorContext.
        /// </summary>
        internal NavigationHistory(PortableRazorContext context)
        {
            m_Context = context;
            Log = new Logging(GetType().Name);
        }

        /// <summary>
        /// Called by the Context classes.
        /// Called when navigating to a now view while saving to previous state.
        /// </summary>
        /// <param name="item"></param>
        internal void AddHistoryItem(NavigationHistoryItem item)
        {
            m_History.Push(item);
        }

        /// <summary>
        /// Called by the Context classes.
        /// Called when navigating to a new view without saving the old state.
        /// </summary>
        /// <param name="item"></param>
        /// <returns>the replaces item that HAS TO BE disposed</returns>
        internal NavigationHistoryItem ReplaceCurrent(NavigationHistoryItem item)
        {
            var current = m_History.Pop();
            m_History.Push(item);
            return current;
        }

        /// <summary>
        /// Checks if it's possible to go back or not.
        /// </summary>
        /// <returns></returns>
        public bool CanGoBack => m_History.Count > 1; // top is the current one

        /// <summary>
        /// Number of elements in the history (behind the currently visible one)
        /// </summary>
        public int Count => m_History.Count - 1;

        /// <summary>
        /// Go back to the last stored action.
        /// This assumes you stay inside your application and call no external links.
        /// </summary>
        public async Task<bool> GoBackAsync()
        {
            // not possible
            if (!CanGoBack) return true; // ignore the call.. this is no error (at least not at this level but there is something wrong with your presentation)

            var current = m_History.Pop();
            
            // navigate
            bool success = await m_History.Peek().CallNavigateBackAsync();

            // drop the current one
            current.Dispose();

            if (!success)
            { 
                Log.Error("[GoBack] failed to go back to: " + m_History.Peek());
                return false;
            }

            Log.Info("[GoBack] got back to: " + m_History.Peek());
            return true;
        }

        /// <summary>
        /// Removes all entries from the history.
        /// Use this to clear after the user hits the home button for example.
        /// </summary>
        public void ClearHistory()
        {
            // TODO [ClearHistory] not sure about this.. need a some use cases to define how this should behave correctly

            var current = m_History.Pop();

            while (m_History.Count > 0)
            {
                var item = m_History.Pop();
                item.Dispose();
            }
            
            m_History.Push(current);
        }
    }
}
