using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using PortableRazor.Json;
using PortableRazor.Web.Html;

namespace PortableRazor
{
	//This was copied from the generated .cs for one of the razor views
	public abstract class RazorRenderable : RegisteredObject
    {
	    public class RazorViewRenderer : IDisposable
	    {
            /// <summary>
            /// Rendering of the main content.
            /// </summary>
	        public readonly TextWriter Main = new System.IO.StringWriter();

            /// <summary>
            /// Building a init script that will be started when the view is shown.
            /// </summary>
            //public readonly TextWriter LoadScript = new System.IO.StringWriter();

	        public event Action LoadScripts;
	        public void Dispose()
	        {
	            Main.Dispose();
	        }

	        public void RunLoadScripts()
	        {
	            LoadScripts?.Invoke();
	        }
	    }

        /// <summary>
        /// Writer that is used to generate the HTML page during Generation.
        /// It is only available during generation and null elsewise.
        /// In general it's only required while rendering elements with range, like a form, but not for something like a "span"
        /// </summary>
        public RazorViewRenderer RazorWriter { get; private set; }


	    public bool Generating => _Generating;

        // ATTENTION: we are assuming that only one renderable is rendering at a time
        // at the moment the system is not capable of multiple renderings at the same time
        // BUT if you use multiple windows this can be a problem (in this case, this flag should be part of the context.. at least)
	    private static bool _Generating = false;

        // This method is OPTIONAL
        //
        /// <summary>Executes the template and returns the output as a string.</summary>
        /// <returns>The template output.</returns>
        public HtmlString GenerateString()
        {
            using (var sw = new RazorViewRenderer())
            {
                Generate(sw);
                return sw.Main.ToString();
            }
        }

        public async Task<HtmlString> GenerateStringAsync()
        {
            return await Task.Run(() =>
            {
                using (var sw = new RazorViewRenderer())
                {
                    Generate(sw);
                    return sw.Main.ToString();
                }
            });
        }

        public async Task<RazorViewRenderer> GenerateAsync()
	    {
            var sw = new RazorViewRenderer();
            await Task.Run(() => Generate(sw));
            return sw;
	    }

        // This method is OPTIONAL, you may choose to implement Write and WriteLiteral without use of RazorWriter
        // and provide another means of invoking Execute.
        //
        /// <summary>Executes the template, writing to the provided text writer.</summary>
        /// <param name="writer">The TextWriter to which to write the template output.</param>
        public void Generate(RazorViewRenderer writer)
		{
            _Generating = true;
            RazorWriter = writer;

            // check for a layout
		    try
		    {
		        Execute();
		    }
		    catch (Exception ex)
		    {
		        Debug.WriteLine(ex.Message);
		    }

			RazorWriter = null;
            _Generating = false;
        }

		// This method is REQUIRED, but you may choose to implement it differently
		//
		/// <summary>Writes a literal value to the template output without HTML escaping it.</summary>
		/// <param name=""value"">The literal value.</param>
		protected void WriteLiteral(string value)
		{
			RazorWriter.Main.Write(value);
		}

		// This method is REQUIRED if the template contains any Razor helpers, but you may choose to implement it differently
		//
		/// <summary>Writes a literal value to the TextWriter without HTML escaping it.</summary>
		/// <param name=""writer"">The TextWriter to which to write the literal.</param>
		/// <param name=""value"">The literal value.</param>
		protected static void WriteLiteralTo (RazorViewRenderer writer, string value)
		{
			writer.Main.Write(value);
		}

		// This method is REQUIRED, but you may choose to implement it differently
		//
		/// <summary>Writes a value to the template output, HTML escaping it if necessary.</summary>
		/// <param name=""value"">The value.</param>
		/// <remarks>The value may be a Action<System.IO.TextWriter>, as returned by Razor helpers.</remarks>
		protected void Write (object value)
		{
			WriteTo(RazorWriter, value);
		}

        // This method is REQUIRED if the template contains any Razor helpers, but you may choose to implement it differently
        //
        /// <summary>Writes an object value to the TextWriter, HTML escaping it if necessary.</summary>
        /// <param name=""writer"">The TextWriter to which to write the value.</param>
        /// <param name=""value"">The value.</param>
        /// <remarks>The value may be a Action<System.IO.TextWriter>, as returned by Razor helpers.</remarks>
        protected static void WriteTo(RazorViewRenderer writer, object value)
		{
			if (value == null)
				return;

			var write = value as Action<RazorViewRenderer>;
			if (write != null) {
				write(writer);
				return;
			}

			if (value is HtmlString)
				writer.Main.Write(value);
			else
				writer.Main.Write(value.ToString());
				//writer.Main.Write(System.Net.WebUtility.HtmlEncode (value.ToString ()));
		}

		// This method is REQUIRED, but you may choose to implement it differently
		//
		/// <summary>
		/// Conditionally writes an attribute to the template output.
		/// </summary>
		/// <param name=""name"">The name of the attribute.</param>
		/// <param name=""prefix"">The prefix of the attribute.</param>
		/// <param name=""suffix"">The suffix of the attribute.</param>
		/// <param name=""values"">Attribute values, each specifying a prefix, value and whether it's a literal.</param>
		protected void WriteAttribute(string name, string prefix, string suffix, params Tuple<string,object,bool>[] values)
		{
			WriteAttributeTo(RazorWriter, name, prefix, suffix, values);
		}

		// This method is REQUIRED if the template contains any Razor helpers, but you may choose to implement it differently
		//
		/// <summary>
		/// Conditionally writes an attribute to a TextWriter.
		/// </summary>
		/// <param name=""writer"">The TextWriter to which to write the attribute.</param>
		/// <param name=""name"">The name of the attribute.</param>
		/// <param name=""prefix"">The prefix of the attribute.</param>
		/// <param name=""suffix"">The suffix of the attribute.</param>
		/// <param name=""values"">Attribute values, each specifying a prefix, value and whether it's a literal.</param>
		///<remarks>Used by Razor helpers to write attributes.</remarks>
		protected static void WriteAttributeTo(RazorViewRenderer writer, string name, string prefix, string suffix, params Tuple<string,object,bool>[] values)
		{
			// this is based on System.Web.WebPages.WebPageExecutingBase
			// Copyright (c) Microsoft Open Technologies, Inc.
			// Licensed under the Apache License, Version 2.0
			if (values.Length == 0) {
				// Explicitly empty attribute, so write the prefix and suffix
				writer.Main.Write(prefix);
				writer.Main.Write(suffix);
				return;
			}

			bool first = true;
			bool wroteSomething = false;

			for (int i = 0; i < values.Length; i++) {
				Tuple<string,object,bool> attrVal = values [i];
				string attPrefix = attrVal.Item1;
				object value = attrVal.Item2;
				bool isLiteral = attrVal.Item3;

				if (value == null) {
					// Nothing to write
					continue;
				}

				// The special cases here are that the value we're writing might already be a string, or that the 
				// value might be a bool. If the value is the bool 'true' we want to write the attribute name instead
				// of the string 'true'. If the value is the bool 'false' we don't want to write anything.
				//
				// Otherwise the value is another object (perhaps an string), and we'll ask it to format itself.
				string stringValue;
				bool? boolValue = value as bool?;
				if (boolValue == true) {
					stringValue = name;
				} else if (boolValue == false) {
					continue;
				} else {
					stringValue = value as string;
				}

				if (first) {
					writer.Main.Write (prefix);
					first = false;
				} else {
					writer.Main.Write (attPrefix);
				}

				if (isLiteral) {
					writer.Main.Write (stringValue ?? value);
				} else {
					WriteTo (writer, stringValue ?? value);
				}
				wroteSomething = true;
			}
			if (wroteSomething) {
				writer.Main.Write (suffix);
			}
		}

		// This method is REQUIRED. The generated Razor subclass will override it with the generated code.
		//
		///<summary>Executes the template, writing output to the Write and WriteLiteral methods.</summary>.
		///<remarks>Not intended to be called directly. Call the Generate method instead.</remarks>
		public abstract void Execute();

	    public abstract HtmlString RenderBindingScript();
    }
}

