﻿using System;
using System.IO;
using System.Reflection;

namespace PortableRazor
{
    public abstract class BaseResourceManager
    {
        /// <summary>
        /// All embedded resources to extract are extracted to that path.
        /// This property is static because there should be only one folder with web content.
        /// It also allows the webviews to get this path.
        /// </summary>
        public static string ContentPath { get; protected set; }

        public void ExtractResources(Assembly assembly, String[] embeddedContentFolders)
        {
            // sort the folders by length to always check the deepest folder first
            Array.Sort(embeddedContentFolders, (s1, s2) => s2.Length - s1.Length);

            // based on the given paths we can compute all valid prefixes of embedded resources to extract
            var assmeblyName = assembly.GetName().Name;
            String[] resourceIdentifierPrefixes = new string[embeddedContentFolders.Length];
            for (int i = 0; i < embeddedContentFolders.Length; i++)
            {
                var prefix = embeddedContentFolders[i];
                prefix = prefix.Replace('-', '_');
                prefix = prefix.Replace('/', '.');
                prefix = prefix.Replace('\\', '.');
                if (!prefix.StartsWith(".")) prefix = "." + prefix;
                if (!prefix.EndsWith(".")) prefix = prefix + ".";

                resourceIdentifierPrefixes[i] = assmeblyName + prefix;
            }

            // iterate over all embedded resources
            foreach (var resource in assembly.GetManifestResourceNames())
                for (int i = 0; i < embeddedContentFolders.Length; i++)
                    if (resource.StartsWith(resourceIdentifierPrefixes[i]))
                    {
                        var filePath = Path.Combine(ContentPath, embeddedContentFolders[i], resource.Substring(resourceIdentifierPrefixes[i].Length));
                        ExtractResource(assembly, filePath, resource);
                        break;
                    }

        }

        protected abstract void ExtractResource(Assembly assembly, string filePath, string resource);

        /* this can be used as reference implementation!
        {
            // in case files change, this is not a good idea
            // todo: find somekind of timestamp or hash check (if that is cheaper)
            // if (File.Exists(filePath))
            //     return;

            // create the folder if not existing
            filePath = filePath.Replace("\\", "/");
            var directoryName = filePath.Substring(0, filePath.LastIndexOf("/"));
            if (!Directory.Exists(directoryName))
                Directory.CreateDirectory(directoryName);

            // open embedded file stream and target file stream
            var input = ResourceLoader.GetEmbeddedResourceStream(assembly, resource);
            var output = new FileStream(filePath, FileMode.OpenOrCreate);

            // copy input to output
            input.CopyTo(output);
            output.Flush();

            // close streams
            output.Dispose();
            input.Dispose();
        }*/
    }
}
