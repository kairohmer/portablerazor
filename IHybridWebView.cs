using System;
using System.Threading.Tasks;

namespace PortableRazor
{
    public class RoutingEventArgs : EventArgs
    {
        /// <summary>
        /// Result after the context processed the request.
        /// True means the application can handle the request.
        /// </summary>
        public bool Handled { get; set; }

        /// <summary>
        /// The requested routing target.
        /// </summary>
        public readonly Uri Target;

        public RoutingEventArgs(Uri target)
        {
            Target = target;
        }
    }

	public interface IHybridWebView
	{
        /// <summary>
        /// The context of this webview.
        /// It's the main API access point that offers pretty much everything (that is implemented).
        /// </summary>
        PortableRazorContext RazorContext { get; } // the platform implementation simply create a PortableRazorContext (or maybe a derived version)

		string BasePath { get; }

		void LoadHtmlString(string html);
        
		Task<string> EvaluateJavascriptAsync(string script);

	    event EventHandler<string> NotifyFromJavascript;
	    event EventHandler<RoutingEventArgs> RoutingRequest;
	}
}