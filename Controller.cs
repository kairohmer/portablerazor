﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using PortableRazor.Web.Binding;
using PortableRazor.Web.Mvc;

namespace PortableRazor
{
    public class ControllerViewBag : IViewBag
    {
        public dynamic Model { get; set; }
        public string Titel { get; set; }
    }

    /// <summary>
    /// Base Controller.
    /// Deriving class names have to end with 'Controller'.
    /// Views for this controller have to be in namespace: <Assembly>.Views.<ControllerName>.
    /// </summary>
    public abstract class Controller : IDisposable
    {
        //---------------------------------------------------------------------
        // register all views to allow partial views and layouts

        private static readonly Dictionary<string, Type> s_RegisteredViews = new Dictionary<string, Type>();
        private static readonly List<string> s_RegisteredAssembies = new List<string>();

        /// <summary>
        /// Called during the controller registration.
        /// Register all views to allow partial views and layouts
        /// </summary>
        /// <typeparam name="T"></typeparam>
        internal static void RegisterViews<T>() where T : Controller
        {
            var assembly = typeof(T).GetTypeInfo().Assembly;

            // only once per assmebly
            if (s_RegisteredAssembies.Contains(assembly.FullName)) return;
            s_RegisteredAssembies.Add(assembly.FullName);

            // register all view types
            var viewBaseTypeInfo = typeof(MvcViewBase).GetTypeInfo();
            foreach (var type in assembly.DefinedTypes)
            {
                if (viewBaseTypeInfo.IsAssignableFrom(type))
                    RegisterView(type.AsType());
            }
        }

        private static void RegisterView(Type viewType)
        {
            string key = viewType.FullName;
            var firstDot = key.IndexOf('.');
            var secondDot = key.Substring(firstDot+1).IndexOf('.');

            // force convention
            if (secondDot < 0 || !key.Substring(firstDot + 1, secondDot).Equals("Views"))
                throw new Exception("[Controller] File Convention Error: All your Views have be in the namespace '<Assembly>.Views'.");

            // the key is "<ControllerName>.ViewName"
            // exceptions are partial views and layouts
            key = key.Substring(firstDot + secondDot + 2);

            if (!s_RegisteredViews.ContainsKey(key))
                s_RegisteredViews.Add(key, viewType);
        }

        internal static Type GetRegisteredView(string typeName)
        {
            if (!s_RegisteredViews.ContainsKey(typeName)) return null;
            return s_RegisteredViews[typeName];
        }

        //---------------------------------------------------------------------
        
        protected PortableRazorContext Context { get; private set; }

        /// <summary>
        /// Used to pass the model to the view.
        /// It is passed to each rendered view.
        /// Note, that the model is reset to null after rendering.
        /// </summary>
        protected readonly ControllerViewBag ViewBag;

        /// <summary>
        /// Name of the Controller is the class name without the 'Controller' in the end.
        /// </summary>
        public readonly string Name;

        /// <summary>
        /// Layout to use by default. If no Layout with this name is found, none is used.
        /// You can set it null or empty to disable layouts by default (for this controller).
        /// </summary>
        protected readonly string DefaultLayout = "_Layout";

        internal static Controller CreateController(PortableRazorContext context, MvcContext.RegisteredController controllerDescription)
        {
            var instance = (Controller) Activator.CreateInstance(controllerDescription.ControllerType, controllerDescription.AppContext, controllerDescription.Name);
            instance.Context = context;
            return instance;
        }

        /// <summary>
        /// Creates a new controller.
        /// Make sure deriving classes provide the same constructor signature.
        /// To initialize a deriving controller use the constructor.
        /// Note that navigation related properties like the 'PortableRazorContext' are not available at this point.
        /// Besides application specific initialization, the 'DefaultLayout' can be specified here.
        /// </summary>
        /// <param name="appContext">provides access to the application, can be cast into the type passed during controller registration</param>
        /// <param name="name">controller name</param>
        protected Controller(ApplicationContext appContext /* not used but for convenient deriving constructor generation */, string name)
        { 
            Name = name;

            // view bag
            ViewBag = new ControllerViewBag
            {
                Titel = Name,  // default page title
                Model = null
            };
        }

        public abstract void Dispose();

        /// <summary>
        /// Shows a specific view using a specific layout.
        /// </summary>
        /// <param name="viewName">name of the view to show</param>
        /// <param name="layoutName">name of the layout to use</param>
        protected void View(string viewName, string layoutName)
        {
            Context.Mvc.Navigate(this, viewName, layoutName, ViewBag);
        }

        /// <summary>
        /// Shows the view that has the same name as the calling method of the controller.
        /// Uses the DefaultLayout of this controller or none, if the DefaultLayout was set null or empty.
        /// </summary>
        /// <param name="viewName">is set automatically</param>
        protected void View([CallerMemberName] string viewName = "")
        {
            View(viewName, DefaultLayout);
        }

        /// <summary>
        /// Evaluates java script code.
        /// </summary>
        /// <param name="javascriptCode"></param>
        /// <returns></returns>
        protected async Task<string> EvaluateJavascriptAsync(String javascriptCode)
        {
            return await Context.WebView.EvaluateJavascriptAsync(javascriptCode);
        }

        /// <summary>
        /// One action that every controller has.
        /// It is never called so it makes so sense to override it.
        /// </summary>
        protected void NavigateBack()
        {
            var task = Context.History.GoBackAsync();
        }
    }
}
