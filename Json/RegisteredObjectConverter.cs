﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace PortableRazor.Json
{
    public class RegisteredObjectConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var regObj = value as RegisteredObject;
            if (regObj == null) return;

            writer.WriteStartObject();
            writer.WritePropertyName("Handle");
            serializer.Serialize(writer, regObj.Handle);
            writer.WriteEndObject();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject jsonObject = JObject.Load(reader);
            var properties = jsonObject.Properties().ToList();
            foreach (var property in properties)
            {
                if (property.Name == "Handle")
                {
                    Guid guid = property.Value.ToObject<Guid>();
                    return RegisteredObject.GetByGuid(guid);
                }
            }

            return null;
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof(RegisteredObject).GetTypeInfo().IsAssignableFrom(objectType.GetTypeInfo());
        }
    }
}
