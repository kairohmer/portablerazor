﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PortableRazor.Utilities;
using PortableRazor.Utilities.Extensions;

namespace PortableRazor.Json
{
    [JsonConverter(typeof(RegisteredObjectConverter))]
    public class RegisteredObject : IDisposable
    {
        [JsonIgnore]
        protected readonly Logging Log;

        /// <summary>
        /// Store all instances in one place that is easy to access by guid.
        /// </summary>
        private static readonly Dictionary<Guid, RegisteredObject> RegisteredInstances = new Dictionary<Guid, RegisteredObject>();

        /// <summary>
        /// Allows the system to get registered objects by a unique id.
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        internal static RegisteredObject GetByGuid(Guid guid)
        {
            if (RegisteredInstances.ContainsKey(guid))
                return RegisteredInstances[guid];

            return null;
        }

        internal static T GetByGuid<T>(Guid guid) where T : RegisteredObject
        {
            if (RegisteredInstances.ContainsKey(guid))
                return RegisteredInstances[guid] as T;

            return null;
        }

        /// <summary>
        /// Unique handle of this instance.
        /// </summary>
        public readonly Guid Handle;

        protected RegisteredObject()
        {
            Log = new Logging(GetType().NameWithGenerics(true), LogLevel.Verbose);

            // Create a new handle and make sure we have no collision (should not happen so fast)
            Handle = Guid.NewGuid();
            while (RegisteredInstances.ContainsKey(Handle))
                Handle = Guid.NewGuid();
            
            // register the instance
            if(!RegisteredInstances.ContainsKey(Handle))
                RegisteredInstances.Add(Handle, this);

            Log.Verbose($"Created. ({RegisteredInstances.Count} active instances)");
        }

        public virtual void Dispose()
        {
            // unregister the instance
            var instance = RegisteredInstances[Handle];
            RegisteredInstances.Remove(Handle);

            Log.Verbose($"Disposed.({RegisteredInstances.Count} active instances)");
        }
    }
}
