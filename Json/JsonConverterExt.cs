﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace PortableRazor.Json
{
    public static class JsonConverterExt
    {
        /// <summary>
        /// Uses Json to parse a string.
        /// This is meant for primitive types that can be handled by json (typically everything that is passed from input elements).
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="text"></param>
        /// <param name="outParsed"></param>
        /// <returns></returns>
        public static bool TryParse<T>(this string text, out T outParsed)
        {
            try
            {
                // json parsing
                dynamic deserialized = JsonConvert.DeserializeObject("{ \"val\": \"" + text + "\"}");
                outParsed = deserialized.val.ToObject<T>();
                return true;
            }
            catch (Exception) // parsing failed.. this is probably fine
            {
                // more like info.. 
                //throw new Exception($"Parsing of '{value}' failed: " + ex.Message);
                outParsed = default(T);
                return false;
            }
        }
    }
}
