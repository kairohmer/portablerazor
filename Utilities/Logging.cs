using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PortableRazor.Utilities
{
    public enum LogLevel
    {
        Verbose = 0,
        Info = 1,
        Warning = 2,
        Error = 3
    }

    public interface ILogWriter
    {
        void Print(LogLevel level, string message);
    }

    public class Logging
    {
        /// <summary>
        /// Name of the logging component.
        /// </summary>
        public readonly string Name;

        /// <summary>
        /// Min log level to display.
        /// </summary>
        public LogLevel Level;

        public Logging(string name, LogLevel level = LogLevel.Info)
        {
            Name = name;
            Level = level;
        }

        public void Error(string message, Exception exeption, [CallerFilePath] string callerFilePath = "", [CallerLineNumber] int line = 0)
        {
            while (exeption.InnerException != null)
                exeption = exeption.InnerException;

            Write(LogLevel.Error, string.Format("{1}{0}({2}){0}({3} [Line: {4}]){0}{5}", Environment.NewLine + "  ", message, exeption.Message, callerFilePath, line, exeption.StackTrace));
        }

        public void Error(string message, bool printCallerInfo = true, [CallerFilePath] string callerFilePath = "", [CallerLineNumber] int line = 0)
        {
            // no general way to get a stack trace?
            Write(LogLevel.Error, printCallerInfo ? $"{message}{Environment.NewLine}  ({callerFilePath} [Line: {line}])" : $"{message}");
        }


        public void Warning(string message, bool printCallerInfo = true, [CallerFilePath] string callerFilePath = "", [CallerLineNumber] int line = 0)
        {
            Write(LogLevel.Warning, printCallerInfo ? $"{message}{Environment.NewLine}  ({callerFilePath} [Line: {line}])" : $"{message}");
        }


        public void Info(string message)
        {
            Write(LogLevel.Info, message);
        }

        public void Verbose(string message)
        {
            Write(LogLevel.Verbose, message);
        }

        public void Write(LogLevel level, string message)
        {
            if((int)level < (int)Level) return;
            InvokeWrite(level, $"[{Name}] {message}");
        }


        //---------------------------------------------------------------------
        // Static parts 
        //---------------------------------------------------------------------


        public static void Register(ILogWriter writer)
        {
            if(s_RegisteredWriters.Contains(writer)) return;
            s_RegisteredWriters.Add(writer);
        }

        private static readonly List<ILogWriter> s_RegisteredWriters = new List<ILogWriter>(4);


        private static readonly SemaphoreSlim s_Semaphore = new SemaphoreSlim(1, 1);
        private static void InvokeWrite(LogLevel level, string message)
        {
            s_Semaphore.Wait();

            try
            {
                foreach (var writer in s_RegisteredWriters)
                {
                    writer.Print(level, message);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            s_Semaphore.Release();

        }
    }
}
