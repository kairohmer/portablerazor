﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PortableRazor.Utilities.Extensions
{
    public static class TypeExt
    {
        /// <summary>
        /// Another ToString method that displays the generic type name.
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public static string NameWithGenerics(this Type t, bool fullName = false)
        {
            if (t.GenericTypeArguments.Length == 0)
                return fullName ? t.FullName : t.Name;

            String name = (fullName ? t.FullName : t.Name).Substring(0, t.Name.Length - 2) + "<";

            for (int i = 0; i < t.GenericTypeArguments.Length - 1; i++)
            {
                name += t.GenericTypeArguments[i].FullName + ", ";
            }

            name += t.GenericTypeArguments[t.GenericTypeArguments.Length - 1].FullName + ">";
            return name;
        }

        public static string NameWithAncestors(this Type t, bool fullName = false)
        {
            String name = t.NameWithGenerics(fullName);

            t = t.GetTypeInfo().BaseType;
            if (t == null) return name;

            string parents = "";
            while (t != null)
            {
                parents = t.NameWithGenerics() + "::" + parents;
                t = t.GetTypeInfo().BaseType;
            }
            name += " (" + parents.Substring(0, parents.Length-2) + ")";

            return name;
        }

        public static object GetDefault(this Type type)
        {
            if (type.GetTypeInfo().IsValueType)
            {
                return Activator.CreateInstance(type);
            }
            return null;
        }
    }
}
