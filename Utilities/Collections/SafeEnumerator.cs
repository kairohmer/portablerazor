﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace PortableRazor.Utilities.Collections
{
    // http://www.codeproject.com/Articles/56575/Thread-safe-enumeration-in-C
    public class SafeEnumerator<T> : IEnumerator<T>
    {
        // this is the (thread-unsafe)
        // enumerator of the underlying collection
        private readonly IEnumerator<T> m_Inner;
        // this is the object we shall lock on. 
        private readonly SemaphoreSlim m_Lock;

        public SafeEnumerator(IEnumerator<T> inner, SemaphoreSlim @lock)
        {
            m_Inner = inner;
            m_Lock = @lock;
            // entering lock in constructor
            m_Lock.Wait();
        }

        #region Implementation of IDisposable

        public void Dispose()
        {
            // .. and exiting lock on Dispose()
            // This will be called when foreach loop finishes
            m_Lock.Release();
        }

        #endregion

        #region Implementation of IEnumerator

        // we just delegate actual implementation
        // to the inner enumerator, that actually iterates
        // over some collection

        public bool MoveNext()
        {
            return m_Inner.MoveNext();
        }

        public void Reset()
        {
            m_Inner.Reset();
        }

        public T Current
        {
            get { return m_Inner.Current; }
        }

        object IEnumerator.Current
        {
            get { return Current; }
        }

        #endregion
    }

    public class SafeEnumerable<T> : IEnumerable<T>
    {
        private readonly IEnumerable<T> m_Inner;
        private readonly SemaphoreSlim m_Lock;

        public SafeEnumerable(IEnumerable<T> inner, SemaphoreSlim @lock)
        {
            m_Lock = @lock;
            m_Inner = inner;
        }

        #region Implementation of IEnumerable

        public IEnumerator<T> GetEnumerator()
        {
            return new SafeEnumerator<T>(m_Inner.GetEnumerator(), m_Lock);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }

    public static class EnumerableExtension
    {
        public static IEnumerable<T> AsLocked<T>(this IEnumerable<T> ie, SemaphoreSlim @lock)
        {
            return new SafeEnumerable<T>(ie, @lock);
        }
    }
}