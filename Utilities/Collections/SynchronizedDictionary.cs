﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Threading;

namespace PortableRazor.Utilities.Collections
{
    public interface IReadOnlySynchronizedDictionary<TKey, TValue> : IEnumerable<KeyValuePair<TKey, TValue>>, INotifyCollectionChanged, INotifyPropertyChanged
    {
        bool ContainsKey(TKey key);
        TValue this[TKey key] { get; }
        int Count { get; }
    }

    public class SynchronizedDictionary<TKey, TValue> : IReadOnlySynchronizedDictionary<TKey, TValue>
    {
        //---------------------------------------------------------------------
        // Fields
        //---------------------------------------------------------------------

        public readonly SemaphoreSlim SyncRoot = new SemaphoreSlim(1, 1);
        private readonly Dictionary<TKey, TValue> _InnerDict;


        //---------------------------------------------------------------------



        //---------------------------------------------------------------------
        // Constructors
        //---------------------------------------------------------------------

        public SynchronizedDictionary()
        {
            _InnerDict = new Dictionary<TKey, TValue>();
        }

        public SynchronizedDictionary(int capacity)
        {
            _InnerDict = new Dictionary<TKey, TValue>(capacity);
        }


        //---------------------------------------------------------------------


        public bool ContainsKey(TKey key)
        {
            return _InnerDict.ContainsKey(key);
        }

        /// <summary>
        /// Atomic "Add" operation.
        /// Adds the value if the key is not present in the dictionary.
        /// If there already is such a key, the call is ignored.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns>true if the key was added (false means thats there was an value with this key)</returns>
        public bool TryAdd(TKey key, TValue value)
        {
            bool added = false;
            SyncRoot.Wait();
            {
                if (!_InnerDict.ContainsKey(key))
                {
                    _InnerDict.Add(key, value);
                    added = true;
                }
            }
            SyncRoot.Release();

            if (added)
                OnCollectionChanged(NotifyCollectionChangedAction.Add, new KeyValuePair<TKey, TValue>(key, value));

            return added;
        }

        /// <summary>
        /// Atomic "Add" operation.
        /// Adds the value if the key is not present in the dictionary.
        /// If there already is such a key, false will be returned and "presentValue" will contain the current value at this key.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="presentValue">in case there is value for this key, you will get that value (undefined otherwise)</param>
        /// <returns>true if the key was added (false means thats there was an value with this key)</returns>
        public bool TryAdd(TKey key, TValue value, out TValue presentValue)
        {
            bool added = false;
            presentValue = default(TValue);
            SyncRoot.Wait();
            {
                if (!_InnerDict.ContainsKey(key))
                {
                    _InnerDict.Add(key, value);
                    added = true;
                }
                else
                {
                    presentValue = _InnerDict[key];
                }
            }
            SyncRoot.Release();

            if (added)
                OnCollectionChanged(NotifyCollectionChangedAction.Add, new KeyValuePair<TKey, TValue>(key, value));

            return added;
        }

        /// <summary>
        /// Atomic "Remove" operation.
        /// </summary>
        /// <param name="key">the key to remove</param>
        /// <returns>true if removing succeded (false means the value was not present)</returns>
        public bool TryRemove(TKey key)
        {
            bool removed = false;
            SyncRoot.Wait();
            {
                if (_InnerDict.ContainsKey(key))
                {
                    _InnerDict.Remove(key);
                    removed = true;
                }
            }
            SyncRoot.Release();

            if (removed)
                OnCollectionChanged();

            return removed;
        }

        /// <summary>
        /// Atomic "Remove" operation.
        /// </summary>
        /// <param name="key">the key to remove</param>
        /// <returns>true if removing succeded (false means the value was not present)</returns>
        public bool TryRemove(TKey key, out TValue removedValue)
        {
            bool removed = false;
            removedValue = default(TValue);
            SyncRoot.Wait();
            {
                if (_InnerDict.ContainsKey(key))
                {
                    removedValue = _InnerDict[key];
                    _InnerDict.Remove(key);
                    removed = true;
                }
            }
            SyncRoot.Release();

            if (removed)
                OnCollectionChanged();

            return removed;
        }


        /// <summary>
        /// Atomic "Remove" operation.
        /// </summary>
        /// <param name="value">the value to remove (removes all of them if multiple are present)</param>
        /// <returns>true if removing succeded (false means the value was not present)</returns>
        public bool TryRemove(TValue value)
        {
            bool removed = false;
            TKey key;
            SyncRoot.Wait();
            {
                while (_InnerDict.TryGetKey(value, out key))
                {
                    _InnerDict.Remove(key);
                    removed = true;
                }
            }
            SyncRoot.Release();

            if (removed)
                OnCollectionChanged();

            return removed;
        }


        /// <summary>
        /// Atomic "Get" operation.
        /// </summary>
        /// <param name="key">the key to search for</param>
        /// <param name="value">the resulting value (if existing), undefined else wise</param>
        /// <returns>true if the getting the value succeeded (false means that the key was not existing)</returns>
        public bool TryGet(TKey key, out TValue value)
        {
            return _InnerDict.TryGetValue(key, out value); ;
        }

        /// <summary>
        /// Atomic "Get and Remove" operation.
        /// </summary>
        /// <param name="key">the key to search for</param>
        /// <param name="value">the resulting value (if existing), undefined else wise</param>
        /// <returns>true if pulling the value succeeded (false means that the key was not existing)</returns>
        public bool TryGetValue(TKey key, out TValue value)
        {
            bool removed = false;
            value = default(TValue);
            SyncRoot.Wait();
            {
                if (_InnerDict.ContainsKey(key))
                {
                    value = _InnerDict[key];
                    _InnerDict.Remove(key);
                    removed = true;
                }
            }
            SyncRoot.Release();

            if (removed)
                OnCollectionChanged();

            return removed;
        }


        public TValue this[TKey key]
        {
            get
            {
                return _InnerDict[key];
            }
            set
            {
                SyncRoot.Wait();
                {
                    _InnerDict[key] = value;
                }
                SyncRoot.Release();
            }
        }

        public void Clear()
        {
            SyncRoot.Wait();
            {
                _InnerDict.Clear();
            }
            SyncRoot.Release();

            OnCollectionChanged();
        }


        public int Count { get { return _InnerDict.Count; } }


        //---------------------------------------------------------------------
        // Enumerators
        //---------------------------------------------------------------------

        /// <summary>
        /// Locks the Collection for iterations.
        /// DO NOT TRY TO CHANGE ANYTHING WITHIN THE COLLECTION.
        /// </summary>
        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            // instead of returning an usafe enumerator,
            // we wrap it into our thread-safe class
            return new SafeEnumerator<KeyValuePair<TKey, TValue>>(_InnerDict.GetEnumerator(), SyncRoot);
        }

        /// <summary>
        /// Locks the Collection for iterations.
        /// DO NOT TRY TO CHANGE ANYTHING WITHIN THE COLLECTION.
        /// </summary>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }


        /// <summary>
        /// Locks the Collection for iterations.
        /// DO NOT TRY TO CHANGE ANYTHING WITHIN THE COLLECTION.
        /// </summary>
        public IEnumerable<TValue> Values
        {
            get { return _InnerDict.Values.AsLocked(SyncRoot); }
        }

        /// <summary>
        /// Locks the Collection for iterations.
        /// DO NOT TRY TO CHANGE ANYTHING WITHIN THE COLLECTION.
        /// </summary>
        public IEnumerable<TKey> Keys
        {
            get { return _InnerDict.Keys.AsLocked(SyncRoot); }
        }


        /*
        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            lock (this.SyncRoot)
            {
                foreach (var element in _InnerDict)
                {
                    yield return element;
                }
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public System.Collections.Generic.IEnumerable<TValue> GetValueEnumerator()
        {
            lock (this.SyncRoot)
            {
                foreach (var element in _InnerDict.Values)
                {
                    yield return element;
                }
            }
        }*/
        /*
        public System.Collections.Generic.IEnumerator<TKey> Keys
        {
            get
            {
                lock (this.SyncRoot)
                {
                    foreach (var element in _InnerDict.Keys)
                    {
                        yield return element;
                    }
                }
            }
        }

        public System.Collections.Generic.IEnumerator<TValue> Values
        {
            get
            {
                lock (this.SyncRoot)
                {
                    foreach (var element in _InnerDict.Values)
                    {
                        yield return element;
                    }
                }
            }
        }*/


        //---------------------------------------------------------------------

        public void KeysCopyTo(TKey[] array, int index = 0)
        {
            _InnerDict.Keys.CopyTo(array, index);
        }

        public void ValuesCopyTo(TValue[] array, int index = 0)
        {
            _InnerDict.Values.CopyTo(array, index);
        }

        /*

    public bool TryGetValue(TKey key, out TValue value)
    {
      throw new NotImplementedException();
    }




    public void Add(KeyValuePair<TKey, TValue> item)
    {
      throw new NotImplementedException();
    }



    public bool Contains(KeyValuePair<TKey, TValue> item)
    {
      throw new NotImplementedException();
    }

    public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
    {
      throw new NotImplementedException();
    }

    public int Count
    {
      get { throw new NotImplementedException(); }
    }

    public bool IsReadOnly
    {
        get { throw new NotImplementedException(); }
    }

    public bool Remove(KeyValuePair<TKey, TValue> item)
    {
        throw new NotImplementedException();
    }

    public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
    {
      throw new NotImplementedException();
    }
        */



        //---------------------------------------------------------------------
        // Observerable
        //---------------------------------------------------------------------

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        public event PropertyChangedEventHandler PropertyChanged;
        private const string CountString = "Count";
        private const string IndexerName = "Item[]";
        private const string KeysName = "Keys";
        private const string ValuesName = "Values";


        private void OnPropertyChanged()
        {
            OnPropertyChanged(CountString);
            OnPropertyChanged(IndexerName);
            OnPropertyChanged(KeysName);
            OnPropertyChanged(ValuesName);
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        private void OnCollectionChanged()
        {
            OnPropertyChanged();
            if (CollectionChanged != null) CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        private void OnCollectionChanged(NotifyCollectionChangedAction action, KeyValuePair<TKey, TValue> changedItem)
        {
            OnPropertyChanged();
            if (CollectionChanged != null) CollectionChanged(this, new NotifyCollectionChangedEventArgs(action, changedItem));
        }

        private void OnCollectionChanged(NotifyCollectionChangedAction action, KeyValuePair<TKey, TValue> newItem, KeyValuePair<TKey, TValue> oldItem)
        {
            OnPropertyChanged();
            if (CollectionChanged != null) CollectionChanged(this, new NotifyCollectionChangedEventArgs(action, newItem, oldItem));
        }

        private void OnCollectionChanged(NotifyCollectionChangedAction action, IList newItems)
        {
            OnPropertyChanged();
            if (CollectionChanged != null) CollectionChanged(this, new NotifyCollectionChangedEventArgs(action, newItems));
        }


        //---------------------------------------------------------------------
    }

    public static class DictionaryExt
    {
        public static bool TryGetKey<TKey, TValue>(this IReadOnlyDictionary<TKey, TValue> dictionary, TValue value, out TKey foundKey)
        {
            foreach (var pair in dictionary)
            {
                if (pair.Value.Equals(value))
                {
                    foundKey = pair.Key;
                    return true;
                }
            }

            foundKey = default(TKey);
            return false;
        }
    }
}