﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;

namespace PortableRazor.Utilities.Collections
{
    public interface IReadOnlySynchronizedList<TValue> : IEnumerable
    {
        bool Contains(TValue value);
        TValue this[int index] { get; }
        int Count { get; }
        new IEnumerator<TValue> GetEnumerator();

        string Print();
        TValue[] ToArray();
    }

    public class SynchronizedList<TValue> : IReadOnlySynchronizedList<TValue>, INotifyCollectionChanged, INotifyPropertyChanged
    {
        public readonly object SyncRoot = new object();
        private readonly List<TValue> _InnerList;

        public SynchronizedList()
        {
            _InnerList = new List<TValue>();
        }

        public SynchronizedList(int initialSize)
        {
            _InnerList = new List<TValue>(initialSize);
        }

        public void Add(TValue value)
        {
            lock (this.SyncRoot)
            {
                _InnerList.Add(value);
            }

            OnCollectionChanged(NotifyCollectionChangedAction.Add, value);
        }

        public bool TryAddIfNotContained(TValue value)
        {
            bool added = false;
            lock (this.SyncRoot)
            {
                if (!_InnerList.Contains(value))
                {
                    _InnerList.Add(value);
                    added = true;
                }
            }

            if (added)
                OnCollectionChanged(NotifyCollectionChangedAction.Add, value);

            return added;
        }

        public bool Contains(TValue value)
        {
            bool contains = true;
            lock (this.SyncRoot)
            {
                contains = _InnerList.Contains(value);
            }
            return contains;
        }

        public bool TryRemove(TValue value)
        {
            bool removed = false;
            lock (this.SyncRoot)
            {
                while (_InnerList.Contains(value))
                {
                    _InnerList.Remove(value);
                    removed = true;
                }
            }

            if (removed)
                OnCollectionChanged();

            return removed;
        }


        public TValue this[int index]
        {
            get
            {
                TValue value;
                lock (this.SyncRoot)
                {
                    value = _InnerList[index];
                }
                return value;
            }
            set
            {
                lock (this.SyncRoot)
                {
                    _InnerList[index] = value;
                }
            }
        }

        public void Clear()
        {
            lock (this.SyncRoot)
            {
                _InnerList.Clear();
            }
            OnCollectionChanged();
        }


        public int Count
        {
            get
            {
                int count = 0;
                lock (this.SyncRoot)
                {
                    count = _InnerList.Count;
                }
                return count;
            }
        }


        public IEnumerator<TValue> GetEnumerator()
        {
            lock (this.SyncRoot)
            {
                foreach (var element in _InnerList)
                {
                    yield return element;
                }
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }


        public void CopyTo(TValue[] array, int index = 0)
        {
            lock (this.SyncRoot)
            {
                _InnerList.CopyTo(array, index);
            }
        }

        public string Print()
        {
            string result = "{ }";
            lock (this.SyncRoot)
            {
                if (_InnerList.Count != 0)
                {
                    result = "{";
                    for (int i = 0; i < _InnerList.Count - 1; i++)
                        result += _InnerList[i].ToString() + ", ";

                    result += _InnerList[_InnerList.Count - 1].ToString() + "}";
                }
            }
            return result;
        }

        public TValue[] ToArray()
        {
            TValue[] result = null;
            lock (this.SyncRoot)
            {
                result = _InnerList.ToArray();
            }
            return result;
        }

        /*
    public ICollection<TKey> Keys
    {
      get { throw new NotImplementedException(); }
    }



    public bool TryGetValue(TKey key, out TValue value)
    {
      throw new NotImplementedException();
    }

    public ICollection<TValue> Values
    {
      get { throw new NotImplementedException(); }
    }


    public void Add(KeyValuePair<TKey, TValue> item)
    {
      throw new NotImplementedException();
    }



    public bool Contains(KeyValuePair<TKey, TValue> item)
    {
      throw new NotImplementedException();
    }

    public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
    {
      throw new NotImplementedException();
    }

    public int Count
    {
      get { throw new NotImplementedException(); }
    }

    public bool IsReadOnly
    {
      get { throw new NotImplementedException(); }
    }

    public bool Remove(KeyValuePair<TKey, TValue> item)
    {
      throw new NotImplementedException();
    }

    public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
    {
      throw new NotImplementedException();
    }
        */

        //---------------------------------------------------------------------
        // Observerable
        //---------------------------------------------------------------------

        public event NotifyCollectionChangedEventHandler CollectionChanged;
        public event PropertyChangedEventHandler PropertyChanged;

        private const string CountString = "Count";
        private const string IndexerName = "Item[]";
        private const string ValuesName = "Values";


        private void OnPropertyChanged()
        {
            OnPropertyChanged(CountString);
            OnPropertyChanged(IndexerName);
            OnPropertyChanged(ValuesName);
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void OnCollectionChanged()
        {
            OnPropertyChanged();
            CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        private void OnCollectionChanged(NotifyCollectionChangedAction action, TValue changedItem)
        {
            OnPropertyChanged();
            CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(action, changedItem));
        }

        private void OnCollectionChanged(NotifyCollectionChangedAction action, TValue newItem, TValue oldItem)
        {
            OnPropertyChanged();
            CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(action, newItem, oldItem));
        }

        private void OnCollectionChanged(NotifyCollectionChangedAction action, IList newItems)
        {
            OnPropertyChanged();
            CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(action, newItems));
        }


        //---------------------------------------------------------------------

    }
}