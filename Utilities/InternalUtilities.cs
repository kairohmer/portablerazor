﻿using System;
using PortableRazor.Json;
using PortableRazor.Web.Binding;

namespace PortableRazor.Utilities
{
    internal static class InternalUtilities
    {
        internal static dynamic[] ObjectArrayToDynamicArray(object[] args)
        {
            dynamic[] dynamicArgs = new dynamic[args.Length];
            for (int i = 0; i < args.Length; ++i)
                dynamicArgs[i] = args[i];

            return dynamicArgs;
        }

        internal static object[] ResolveArgumentValues(dynamic[] args, Type[] argTypes)
        {
            object[] values = new object[args.Length];
            for (int i = 0; i < args.Length; i++)
            {
                try // bindable?
                {
                    var bindable = RegisteredObject.GetByGuid<BindableValueBase>(args[i].Handle);
                    if(bindable == null) continue;
                    values[i] = bindable.GetValue();
                    continue;
                }
                catch (Exception) { } // no problem if this fails

                try // bindable?
                {
                    var bindable = RegisteredObject.GetByGuid<BindableValueBase>(Guid.Parse((string)args[i].Handle));
                    if (bindable == null) continue;
                    values[i] = bindable.GetValue();
                    continue;
                }
                catch (Exception) { } // no problem if this fails

                try // try to deserialize
                {
                    values[i] = args[i].ToObject(argTypes[i]);
                    continue;
                }
                catch (Exception) { } // no problem if this fails


                try // cast by constructor
                {
                    values[i] = Convert.ChangeType(args[i], argTypes[i]);
                    continue; ;
                }
                catch (Exception) { } // no problem if this fails

                values[i] = args[i]; // everything else (probably constant value)
            }

            return values;
        }
    }
}
