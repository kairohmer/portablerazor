﻿using System;
using System.Reflection;
using System.Text;
using PortableRazor.Web.Html;

namespace PortableRazor.Utilities
{
    public static class HtmlUtilities
    {
        public static HtmlString StringToTitleCase(string text)
        {
            text = text.ToLower();
            string[] chunks = text.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            string result = "";
            for (int i = 0; i < chunks.Length; i++)
                result += (i == 0 ? System.String.Empty : " ") + chunks[i].Substring(0, 1).ToUpper() + chunks[i].Substring(1, chunks[i].Length - 1);
            return result;
        }

        public static HtmlString GenerateQueryString(object routeValues = null)
        {
            if (routeValues == null) return String.Empty;

            var qs = new StringBuilder();
            foreach (var property in routeValues.GetType().GetRuntimeProperties())
                qs.AppendFormat("&{0}={1}", property.Name, property.GetMethod.Invoke(routeValues, null));

            if (qs.Length == 0) return String.Empty;

            qs[0] = '?';
            return qs.ToString();
        }
    }
}
