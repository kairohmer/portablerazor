﻿namespace PortableRazor.Utilities
{
    public static class JavaScriptUtilities
    {
        public static string WrapToInlineFunction(string javascript)
        {
            return "(function(){" + javascript + "}).call(this)";
        }
    }
}
