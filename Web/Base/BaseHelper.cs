﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using PortableRazor.Web.Html;

namespace PortableRazor.Web.Base
{
    public abstract class BaseHelper : IDisposable
    {
        protected internal readonly PortableRazorContext Context;

        /// <summary>
        /// The view or viewmodel this BindingHelper belongs to.
        /// </summary>
        protected internal readonly RazorRenderable Host;

        protected BaseHelper(PortableRazorContext context, RazorRenderable host)
        {
            Context = context;
            Host = host;
        }

        public abstract void Dispose();

        protected HtmlString GenerateHtmlAttributes(object htmlAttributes)
        {
            var attrs = new StringBuilder();
            if (htmlAttributes != null)
            {
                foreach (var property in htmlAttributes.GetType().GetRuntimeProperties())
                    attrs.AppendFormat(@" {0}=""{1}""", property.Name.ToLower().Replace('_', '-'), property.GetMethod.Invoke(htmlAttributes, null));
            }
            return attrs.ToString();
        }

    }
}
