﻿using System;
using System.IO;

namespace PortableRazor.Web.Html
{
    /// <summary>
    /// Small helper class that allows to create elements without closing tag.
    /// By writing a using block you can add child elements. E.g. like done by Forms.
    /// </summary>
	public sealed class OpenElement : IDisposable
    {
		private bool _Disposed;
		private readonly RazorRenderable.RazorViewRenderer _Writer;
		private readonly string _ElementName;

		public OpenElement(RazorRenderable.RazorViewRenderer writer, string elementName) {
			_Writer = writer;
			_ElementName = elementName;
		}

	    ~OpenElement()
	    {
	        
	    }

        public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this); 
		}

	    private void Dispose (bool disposing)
		{
			if (!_Disposed) {
				_Disposed = true;
				_Writer.Main.Write($"</{_ElementName}>");
			}
		}
	}
}

