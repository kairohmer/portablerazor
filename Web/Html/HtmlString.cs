﻿namespace PortableRazor.Web.Html
{
    /// <summary>
    /// A simple wrapper string to identify what strings are actually html code.
    /// </summary>
	public class HtmlString
	{
	    private string _Value;

        public HtmlString(string htmlString) { _Value = htmlString; }

        public static implicit operator string(HtmlString htmlString) { return htmlString._Value; }
        public static implicit operator HtmlString(string htmlString) { return new HtmlString(htmlString); }

        public override string ToString ()
		{
			return _Value;
		}
	}
}

