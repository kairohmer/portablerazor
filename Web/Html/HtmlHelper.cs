﻿using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text;
using PortableRazor.Web.Base;
using PortableRazor.Web.Mvc;

namespace PortableRazor.Web.Html
{
    public class PartialViewbag : IViewBag
    {
        public dynamic Model { get; set; }
    }

    public class HtmlHelper : BaseHelper
    {
        public HtmlHelper(PortableRazorContext context, RazorRenderable host) : base(context, host)
        {
        }

        public override void Dispose()
        {
        }

        public HtmlString Partial(string viewName, object model = null)
        {
            var view = Context.Mvc.CreatePartial(viewName, new PartialViewbag() { Model = model });
            if (view == null) return string.Empty;
            return view.GenerateString();
        }



        public HtmlString Raw(string value)
        {
            // missing same escape sequences here?
            return value;
        }

        public static string EscapeHtmlString(string hmlString)
        {
            hmlString = hmlString.Replace("\"", "\\\"");
            hmlString = hmlString.Replace("'", "\\'");
            hmlString = hmlString.Replace("\n", "");
            hmlString = hmlString.Replace("\r", "");
            hmlString = hmlString.Replace("\t", "");
            return hmlString;
        }



        /// <summary>
        /// Appends some helper scripts to the controller page.
        /// </summary>
        /// <returns></returns>
        public HtmlString RenderBindingScript()
        {
            return new HtmlString(
                "<script>\n" +
                "\n" +
                "   function createHandleValuePair(handle, value) {\n" +
                "       var pair = {};\n" +
                "       pair.handle = handle;\n" +
                "       pair.value = value;\n" +
                "       return pair;\n" +
                "   }\n" +
                "\n" +
                "   function htmlEncode(value) { return $('<div/>').text(value).html(); } \n" +
                "   function htmlDecode(value) { return $('<div/>').html(value).text(); } \n" +
                "   function htmlRecode(value) { return htmlDecode(htmlEncode(value)); } \n" +
                "\n" +
                "</script>");
        }
    }
}

