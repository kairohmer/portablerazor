﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortableRazor.Web.Binding;
using PortableRazor.Web.Html;

namespace PortableRazor
{
    public abstract class ViewModelTemplate : ViewModelBase
    {
        // override without doing anything
        protected override void Initialize() {}

        internal static TItemViewModel CreateViewModelForListItem<TItemViewModel, T>(PortableRazorContext context) where TItemViewModel : ViewModelListItemTemplate<T>
        {
            return (TItemViewModel)ViewModelBase.CreateViewModel(context, typeof(TItemViewModel), new object[0]);
        }

        internal static TItemViewModel CreateViewModelForItem<TItemViewModel, T>(PortableRazorContext context) where TItemViewModel : ViewModelItemTemplate<T>
        {
            return (TItemViewModel)ViewModelBase.CreateViewModel(context, typeof(TItemViewModel), new object[0]);
        }
    }

    public abstract class ViewModelListItemTemplate<T> : ViewModelTemplate
    {
        public BindableList<T>.ListItem Item;
    }

    public abstract class ViewModelItemTemplate<T> : ViewModelTemplate
    {
        public T Item;
    }

    public class DefaultListItemTemplate<T> : ViewModelListItemTemplate<T>
    {
        public override void Execute()
        {
            Write($"<div>{Item.Value.ToString()}</div>");
        }
    }

    public class DefaultItemTemplate<T> : ViewModelItemTemplate<T>
    {
        public override void Execute()
        {
            Write($"<div>{Item.ToString()}</div>");
        }
    }

    public class InlineListItemTemplate<T> : ViewModelListItemTemplate<T>
    {
        public Func<BindableList<T>.ListItem, HtmlString> Format;

        public override void Execute()
        {
            Write(Format(Item));
        }
    }

    public class InlineItemTemplate<T> : ViewModelItemTemplate<T>
    {
        public Func<T, HtmlString> Format;

        public override void Execute()
        {
            Write(Format(Item));
        }
    }
}
