﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortableRazor.Web.Mvvm
{
    public class MvvmNavigationHistoryItem : NavigationHistoryItem
    {
        private readonly ViewModelBase m_ViewModel;
        private readonly BindableViewModelSlot m_Slot;

        public MvvmNavigationHistoryItem(ViewModelBase target, BindableViewModelSlot slot)
        {
            m_ViewModel = target;
            m_Slot = slot;
        }

        public override void Dispose()
        {
            m_ViewModel.Dispose();
        }

        protected override async Task<bool> NavigatedBackAsync()
        {
            await m_Slot.NavigateAsync(m_ViewModel, false);
            return true;
        }
    }
}
