﻿using System;
using System.IO;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PortableRazor.Web.Base;
using PortableRazor.Web.Binding;
using PortableRazor.Web.Html;
using PortableRazor.Web.Mvvm;

namespace PortableRazor
{
    public class NavigationHelper : BaseHelper
    {
        /// <summary>
        /// Helper class that is used for Navigation between ViewModels.
        /// </summary>
        public class NavigationToken
        {
            internal readonly NavigationHelper Navigation;
            internal BindableViewModelSlot Slot;

            internal readonly Type ViewModelType = null;
            internal object[] ModelParameters = null;
            internal Type[] ModelParameterTypes = null;
            internal bool History = true;

            /// <summary>
            /// hidden constructor
            /// </summary>
            internal NavigationToken(NavigationHelper navigation, BindableViewModelSlot slot, Type viewModelType)
            {
                Navigation = navigation;
                Slot = slot;
                ViewModelType = viewModelType;
            }

            /// <summary>
            /// Starts the actual navigation.
            /// ATTENTION: Do not do anything after calling this from ViewModel-Code.
            /// </summary>
            public HtmlString Go()
            {
                if (ViewModelType.GetTypeInfo().IsAbstract)
                {
                    // make sure you navigate to a valid viewmodel representation
                    // its possible to have multiple .cshtml for a ViewModel, so it's required to use them for navigation and not the base class.
                    throw new Exception("ViewModel is abstract. Naviagte to a ViewModel generated from .cshml-File instead.");
                }

                // it's an error if we don't know about a slot.
                if (Slot == null)
                    throw new Exception("View is not displayed in a ViewModelSlot, you need to specify the target slot while navigating.");

                // in case were are currently rendering from Razor
                // we want navigation as as callback that is fired when an element is clicked
                if (Navigation.Host.Generating)
                {

                    dynamic callBag = new
                    {
                        SlotHandle = this.Slot.Handle,
                        ViewModelType = this.ViewModelType,
                        ModelParamterTypes = this.ModelParameterTypes,
                        ModelParamters = this.ModelParameters,
                        History = this.History
                    };

                    string serialized = JsonConvert.SerializeObject(callBag);
                    serialized = WebUtility.UrlEncode(serialized);

                    return $"window.external.notify('razornavigateslot:' + '{serialized}')";
                }

                // in case we are not rendering from Razor, we are in Code
                // in this case, we want navigation happen immediately

                object[] values = Utilities.InternalUtilities.ResolveArgumentValues(Utilities.InternalUtilities.ObjectArrayToDynamicArray(ModelParameters), ModelParameterTypes);
                ViewModelBase instance = ViewModelBase.CreateViewModel(Navigation.Context, ViewModelType, values);

                var notAwaited = Slot.NavigateAsync(instance, History); // the programmer is not allowed to do anything after calling
                return null; // return value is not used anyway
            }
        }

        /// <summary>
        /// Helper class that is used for Navigation between ViewModels.
        /// </summary>
        public class NavigationToken<TView> : NavigationToken where TView : ViewModelBase, new()
        {
            /// <summary>
            /// hidden constructor
            /// </summary>
            internal NavigationToken(NavigationHelper navigation, BindableViewModelSlot slot) : base(navigation, slot, typeof(TView)) { }

            /// <summary>
            /// Allows to specify a BindingSlot to navigate in.
            /// </summary>
            /// <param name="slot">to slot to navigate in</param>
            /// <returns>this navigation object, altered to represent the new information</returns>
            public NavigationToken<TView> In(BindableViewModelSlot slot)
            {
                Slot = slot;
                return this;
            }

            /// <summary>
            /// Adds an entry in the history of the navigation slot and allows to go back to current step.
            /// </summary>
            /// <returns>this navigation object, altered to represent the new information</returns>
            public NavigationToken<TView> NoHistory()
            {
                History = false;
                return this;
            }

            // NOTE there are a lot of generated extensions methods to provide strongly typed navigation
            // see NavigationHelper.NavigationTokenExtension.tt
        }

        public NavigationHelper(PortableRazorContext context, RazorRenderable host) : base(context, host)
        {
        }

        public override void Dispose()
        {
        }

        /// <summary>
        /// --- RAZOR only ---
        /// Creates a new slot to host ViewModels in
        /// </summary>
        /// <param name="slot">slot that is probably a field variable of the parent ViewModel</param>
        /// <returns>Renderable String (RAZOR only)</returns>
        public HtmlString Slot(BindableViewModelSlot slot)
        {
            slot.Bind(this);
            return $"<div id=\"{slot.Id}\"></div>";
        }

        /// <summary>
        /// --- RAZOR and ViewModel-Code ---
        /// Navigate to another Viewmodel and replace the current one or change the Viewmodel in a specified slot (call "In(...)" afterwards)
        /// The returned value can be manipulated in a pipeline style, e.g.: NoHistory() ..
        /// To start (or finish the description of) the actual navigation call "Go()" at the end of the pipeline. 
        /// </summary>
        /// <typeparam name="TView">the view model type (ATTENTION: this has to be a Type generated from .cshtml</typeparam>
        /// <returns>a navigation token, that can be altered in a pipeline fashion. In the end, call Go() to start finish the navigation</returns>
        public NavigationToken<TView> To<TView>() where TView : ViewModelBase, new()
        {
            // we don't need a slot, if we are currently hosted in one, it's clear that we want to navigate in that one
            BindableViewModelSlot slot = (Host as ViewModelBase)?.HostingSlot;
            return new NavigationToken<TView>(this, slot);
        }

        /// <summary>
        /// --- RAZOR and ViewModel-Code ---
        /// If there view models in the navigation history, you can go back.
        /// This method allows to check if it's possible, e.g., to show and hide a back button
        /// </summary>
        /// <param name="slot">optional slot specification to describe which slot should navigate back. If null, the current one is used</param>
        /// <returns>true if it's possible</returns>
        public bool CanGoBack()
        {
            return Context.History.CanGoBack;
        }

        /// <summary>
        /// --- RAZOR and ViewModel-Code ---
        /// If there view models in the navigation history, you can go back by calling this function.
        /// ATTENTION: Do not do anything after calling this from ViewModel-Code.
        /// </summary>
        /// <returns>Renderable String (RAZOR only)</returns>
        public HtmlString Back()
        {
            // in case were are currently rendering from Razor
            // we want navigation as as callback that is fired when an element is clicked
            if (Host.Generating)
            {
                return $"window.external.notify('razornavigatebackslot:')";
            }

            // in case we are not rendering from Razor, we are in Code
            // in this case, we want navigation happen immediately

            Task notAwaited = Context.History.GoBackAsync(); // the programmer is not allowed to do anything after calling
            return ""; // return value is not used anyway
        }
    }
}
