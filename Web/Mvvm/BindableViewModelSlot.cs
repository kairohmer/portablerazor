﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using PortableRazor.Utilities;
using PortableRazor.Web.Binding;
using PortableRazor.Web.Html;

namespace PortableRazor.Web.Mvvm
{


    public class BindableViewModelSlot : BindableBase
    {
        /// <summary>
        /// While coming from the javascript event, we need access to the Navigation (and the Host)
        /// </summary>
        protected internal NavigationHelper Navigation { get; private set; }

        public string Id => $"bindingslot_{Handle}";

        /// <summary>
        /// Called by the NavigationHelper and the MvvmNavigationHistoryItem.
        /// </summary>
        /// <returns></returns>
        internal async Task NavigateAsync(ViewModelBase target, bool saveHistory)
        {
            NavigationHistoryItem toDispose = null;

            // save current state?
            if (saveHistory)
                Navigation.Context.History.AddHistoryItem(new MvvmNavigationHistoryItem(target, this));
            else    // do not save the state -> replace the current history entry
                toDispose = Navigation.Context.History.ReplaceCurrent(new MvvmNavigationHistoryItem(target, this));

            // store the slot at the view model, needed for navigation because potentially, there could be more than one slot
            target.HostingSlot = this;

            // swap the visualization
            await SwapAsync(Navigation.Context, target);

            // free the old one
            toDispose?.Dispose();
        }

        private async Task SwapAsync(PortableRazorContext context, ViewModelBase target)
        {
            var writer = await target.GenerateAsync();
            string hmlString = HtmlHelper.EscapeHtmlString(writer.Main.ToString());

            string javascriptCode =
                "function swapViewModel(){ \n" +
                $"   var mainDiv = $('#{Id}'); \n" +
                $"  mainDiv.html(htmlRecode(\"{hmlString}\")); \n" +
                "} \n" +
                "swapViewModel(); \n";

            await context.WebView.EvaluateJavascriptAsync(javascriptCode);
            writer.RunLoadScripts();
        }

        public void Bind(NavigationHelper navigation)
        {
            Navigation = navigation;
        }
    }
}
