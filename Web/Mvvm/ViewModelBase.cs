﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using PortableRazor.Web.Binding;
using PortableRazor.Web.Html;
using PortableRazor.Web.Mvvm;

namespace PortableRazor
{
    public abstract class ViewModelBase : RazorRenderable
    {
        public PortableRazorContext Context { get; private set; }

        internal BindableViewModelSlot HostingSlot = null;

        public HtmlHelper Html { get; private set; }
        protected BindingHelper Binding { get; private set; }
        protected BindingHelperEvents Events => Binding.Events;
        protected NavigationHelper Navigation { get; private set; }

        private readonly List<IDisposable> m_AutoDispose = new List<IDisposable>(10);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context">the current razor context</param>
        /// <param name="viewModelType">a view model type</param>
        /// <param name="modelParameters">parameters to specify the content of this particular instance</param>
        /// <returns></returns>       
        internal static ViewModelBase CreateViewModel(PortableRazorContext context, Type viewModelType, object[] modelParameters)
        {
            if (viewModelType.GetTypeInfo().IsAbstract)
            {
                // make sure you navigate to a valid viewmodel representation
                // its possible to have multiple .cshtml for a ViewModel, so it's required to use them for navigation and not the base class.
                throw new Exception("ViewModel is abstract. Navigate to a ViewModel generated from .cshml-File instead.");
            }

            var view = (ViewModelBase)Activator.CreateInstance(viewModelType);
            view.InitializeView(context);

            // it can make sense to check for types of the init functions and to cache references to the methods to call
            // at the moment we only have two init function: one without parameters and one set of parameters specified by the generic type arguments.
            // dictionary key
            //string key = $"{viewModelType.Name}({string.Join(",", modelParameterTypes.Select((t) => t.Name))})";

            // default init? no parameters.. no try catch because here it's important to see an error
            if (modelParameters.Length == 0)
            {
                view.Initialize();
            }

            // init with parameters.. we just try them.. usually there is only one 
            else
            {
                var initMethods = view.GetType().GetTypeInfo().BaseType.GetTypeInfo().GetDeclaredMethods("Initialize");
                foreach (var initMethod in initMethods)
                {
                    if (initMethod.GetParameters().Length == modelParameters.Length)
                    {
                        try
                        {
                            initMethod.Invoke(view, modelParameters);
                            break;
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine(ex.Message);
                        }
                    }
                }
            }
            return view;
        }

        /// <summary>
        /// Allows to register custom content writer.
        /// </summary>
        /// <param name="context"></param>
        private void InitializeView(PortableRazorContext context)
        {
            Context = context;
            Html = new HtmlHelper(Context, this);
            Binding = new BindingHelper(Context, this);
            Navigation = new NavigationHelper(Context, this);
        }


        /// <summary>
        /// Allows to clean up objects created in the .cshtml file.
        /// Instead of classing only 'new FancyClassName(...)' call 'AutoDispose(new FancyClassName(...))' instead.
        /// The object is then disposed when the view model gets disposed.
        /// </summary>
        /// <typeparam name="T">type implementing IDisposable</typeparam>
        /// <param name="disposable">instance created in .cshtml</param>
        /// <returns>the instance passed as argument, not altered</returns>
        public T AutoDispose<T>(T disposable) where T : IDisposable
        {
            m_AutoDispose.Add(disposable);
            return disposable;
        }

        public override void Dispose()
        {
            Html.Dispose();
            Binding.Dispose();
            Navigation.Dispose();

            foreach(var toDispose in m_AutoDispose)
                toDispose.Dispose();
            m_AutoDispose.Clear();

            base.Dispose();
        }

        /// <summary>
        /// Called immediately after creation.
        /// Arguments:  none in this case. You need to init your model with default values.
        ///             You also can derive from a generic version of this type to get type safe initialization.
        /// </summary>
        protected virtual void Initialize()
        {
            throw new NotImplementedException($"Model '{GetType().Name}' does not implement initialization without parameters.");
        }

        public override HtmlString RenderBindingScript()
        {
            return Html.RenderBindingScript();
        }
    }
}
