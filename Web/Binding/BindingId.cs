﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortableRazor.Web.Html;

namespace PortableRazor.Web.Binding
{
    public class BindingId : BindableBase
    {
        /// <summary>
        /// The Context allows to access the DOM and invoke Javascripts.
        /// </summary>
        private readonly PortableRazorContext _Context;

        /// <summary>
        /// Is called when this Id was attached to an  element by: <element oninput="(at)Binding.OnClick(id, (e) => {...})"/>
        /// and the value of the input 
        /// </summary>
        public event Action<BindingEventArgs> Click;

        /// <summary>
        /// Is called when this Id was attached to an input element by: <input oninput="(at)Binding.OnInput(id, (e) => {...})"/>
        /// and the value of the input 
        /// </summary>
        public event Action<BindingEventArgs> Input;

        /// <summary>
        /// Is called when this Id was attached to an input element by: <input oninput="(at)Binding.OnChange(id, (e) => {...})"/>
        /// and the value of the input 
        /// </summary>
        public event Action<BindingEventArgs> Change;

        /// <summary>
        /// Constructor that is called by the BindingHelper only.
        /// To create an object, call "Binding.GenerateId()".
        /// </summary>
        /// <param name="context"></param>
        internal BindingId(PortableRazorContext context)
        {
            _Context = context;
        }

        /// <summary>
        /// Allows to invoke Java Script code to alter properties of the selected DOM element.
        /// </summary>
        /// <param name="javascript">the result of the evaluated script, if there is any result</param>
        public async Task<string> EvaluateJavascriptAsync(string javascript)
        {
            return await _Context.WebView.EvaluateJavascriptAsync($"$('#{Id}').{javascript}");
        }

        /// <summary>
        /// Get the Id that is used in the DOM.
        /// </summary>
        public String Id => $"id_{Handle}";
        
        public override string ToString()
        {
            return Id;
        }

        /// <summary>
        /// Method called by the PortableRazorContext.
        /// </summary>
        /// <param name="eventName"></param>
        protected internal virtual void Invoke(string eventName)
        {
            switch (eventName)
            {
                case "oninput":     Input?.Invoke(new BindingEventArgs(this, eventName));       return;
                case "onchange":    Change?.Invoke(new BindingEventArgs(this, eventName));      return;
                case "onclick":     Click?.Invoke(new BindingEventArgs(this, eventName));       return;
            }
        }

        // NOTE: Add Extension methods like the ones of "BindingIdExtClass" to provide additional functionality.
    }

    public class BindingEventArgs : EventArgs
    {
        public readonly BindingId Id;
        public readonly string EventName;

        public BindingEventArgs(BindingId id, string eventName)
        {
            Id = id;
            EventName = eventName;
        }
    }

    /// <summary>
    /// A set of methods to change the class of an selected DOM element.
    /// </summary>
    public static class BindingIdExtClass
    {
        /// <summary>
        /// Adds a class to the DOM element.
        /// </summary>
        /// <param name="id">id that was set to DOM element to change</param>
        /// <param name="className">the classname (or multiple ones) to add</param>
        public static async Task AddClassAsync(this BindingId id, string className)
        {
            await id.EvaluateJavascriptAsync($"addClass('{className}')");
        }

        /// <summary>
        /// Removes a class from the DOM element.
        /// </summary>
        /// <param name="id">id that was set to DOM element to change</param>
        /// <param name="className">the classname (or multiple ones) to remove</param>
        public static async Task RemoveClassAsync(this BindingId id, string className)
        {
            await id.EvaluateJavascriptAsync($"removeClass('{className}')");
        }

        /// <summary>
        /// For convenience, this method removes a class (or multiple ones) and add a class (or multiple ones)
        /// </summary>
        /// <param name="id">id that was set to DOM element to change</param>
        /// <param name="toRemove">the classname to add</param>
        /// <param name="toAdd">the classname to remove</param>
        public static async Task ReplaceClassAsync(this BindingId id, string toRemove, string toAdd)
        {
            await RemoveClassAsync(id, toRemove);
            await AddClassAsync(id, toAdd);
        }
    }

    /// <summary>
    /// A set of methods to change the class of an selected DOM element.
    /// </summary>
    public static class BindingIdExtTextHtmlValue
    {
        /// <summary>
        /// Gets the value of the text property of a DOM element.
        /// Get the combined text contents of each element in the set of matched elements, including their descendants.
        /// It basically strips out the html tags and delivers raw unformated text.
        /// </summary>
        /// <param name="id">id that was set to DOM element to change</param>
        public static async Task<string> GetTextAsync(this BindingId id)
        {
            return await id.EvaluateJavascriptAsync($"text()");
        }

        /// <summary>
        /// Sets the value of the text property of a DOM element.
        /// Escapes HTML tags such that they become readable to the user.
        /// </summary>
        /// <param name="id">id that was set to DOM element to change</param>
        /// <param name="text">the value to set</param>
        public static async Task SetTextAsync(this BindingId id, string text)
        {
            await id.EvaluateJavascriptAsync($"text('{text}')"); // TODO escape '
        }

        /// <summary>
        /// Gets the value of the html property of a DOM element.
        /// Gets the content of an element including html tags.
        /// Attention: Some browsers may not return HTML that exactly replicates the HTML source in an original document.
        /// </summary>
        /// <param name="id">id that was set to DOM element to change</param>
        public static async Task<string> GetHtmlAsync(this BindingId id)
        {
            return await id.EvaluateJavascriptAsync($"html()");
        }

        /// <summary>
        /// Sets the value of the text property of a DOM element.
        /// Used to set an element's content, any content that was in that element is completely replaced by the new content.
        /// WARNING: this could be used for code injection, so take care of the sources.
        /// </summary>
        /// <param name="id">id that was set to DOM element to change</param>
        /// <param name="html">the value to set</param>
        public static async Task SetHtmlAsync(this BindingId id, string html)
        {
            await id.EvaluateJavascriptAsync($"html('{html}')"); // TODO escape everything?
        }

        /// <summary>
        /// Gets the value of a DOM element, specifically input elements like 'input', 'textarea' and 'select'.
        /// </summary>
        /// <param name="id">id that was set to DOM element to change</param>
        public static async Task<string> GetValueAsync(this BindingId id)
        {
            return await id.EvaluateJavascriptAsync($"val()");
        }

        /// <summary>
        /// Sets the value of a DOM element, specifically input elements like 'input', 'textarea' and 'select'.
        /// </summary>
        /// <param name="id">id that was set to DOM element to change</param>
        /// <param name="val">the value to set</param>
        public static async Task SetValueAsync(this BindingId id, string val)
        {
            await id.EvaluateJavascriptAsync($"val('{val}')"); // TODO escape '
        }

        /// <summary>
        /// Gets the value of a DOM element, specifically input elements of type 'checkbox'.
        /// </summary>
        /// <param name="id">id that was set to DOM element to change</param>
        public static async Task<bool> GetCheckedAsync(this BindingId id)
        {
            bool result;
            string checkedString = await id.EvaluateJavascriptAsync("prop('checked').toString()");
            if (!Boolean.TryParse(checkedString, out result)) return false;
            return result;
        }

        /// <summary>
        /// Sets the value of a DOM element, specifically input elements of type 'checkbox'.
        /// </summary>
        /// <param name="id">id that was set to DOM element to change</param>
        /// <param name="value">the value to set</param>
        public static async Task SetCheckedAsync(this BindingId id, bool value)
        {
            await id.EvaluateJavascriptAsync($"prop('checked', {value.ToString().ToLower()})");
        }
    }

    /// <summary>
    /// A set of methods to change the class of an selected DOM element.
    /// </summary>
    public static class BindingIdExtContent
    {
        public static async Task AppendAsync(this BindingId id, string html)
        {
            await id.EvaluateJavascriptAsync($"append('{HtmlHelper.EscapeHtmlString(html)}')");
        }

        /// <summary>
        /// Removes a DOM element from the View.
        /// </summary>
        /// <param name="id">id that was set to DOM element to remove</param>
        /// <returns>a task to wait for</returns>
        public static async Task Remove(this BindingId id)
        {
            await id.EvaluateJavascriptAsync("remove()");
        }

        /// <summary>
        /// Changes the visibility of the selected element.
        /// </summary>
        /// <param name="id">id that was set to DOM element to show</param>
        public static async Task Show(this BindingId id)
        {
            await id.EvaluateJavascriptAsync("show()");
        }

        /// <summary>
        /// Changes the visibility of the selected element.
        /// </summary>
        /// <param name="id">id that was set to DOM element to hide</param>
        public static async Task Hide(this BindingId id)
        {
            await id.EvaluateJavascriptAsync("hide()");
        }
    }

    public enum ElementVisibility
    {
        /// <summary>
        /// Default value. The element is visible.
        /// </summary>
        Visible,

        /// <summary>
        /// The element is invisible (but still takes up space)
        /// </summary>
        Hidden,

        /// <summary>
        /// Only for table elements. collapse removes a row or column, but it does not affect the table layout. 
        /// The space taken up by the row or column will be available for other content.
        /// If collapse is used on other elements, it renders as "hidden"
        /// </summary>
        Collapse
    }

    public static class BindingIdExtStyle
    {

        public static async Task SetVisibility(this BindingId id, ElementVisibility visibility)
        {
            await id.EvaluateJavascriptAsync($"css('visibility', '{visibility.ToString().ToLower()}')");
        }
    }
}
