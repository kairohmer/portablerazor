﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortableRazor.Json;

namespace PortableRazor.Web.Binding
{
    /// <summary>
    /// All bindables derive from this class
    /// </summary>
    public abstract class BindableBase : RegisteredObject
    {
        // at the moment there is no common logic, except that 'BindableBase' is used for tracking bindables per view
        // in the future logging could be an important use of this class
    }
}
