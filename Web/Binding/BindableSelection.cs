﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortableRazor.Web.Binding
{
    public abstract class BindableSelection : BindableValueBase
    {
        public static BindableSelection<T> FromEnum<T>(T enumValue = default(T)) where T : struct , IComparable, IFormattable
        {
            var array = Enum.GetValues(typeof(T));
            List<T> list = new List<T>();
            foreach (var item in array)
                list.Add((T)item);

            return new BindableSelection<T>(list, enumValue);
        }
    }

    public class BindableSelection<T> : BindableSelection
    {
        public class SelectionChangedArgs : EventArgs
        {
            public readonly T Selected;
            public readonly int SelectedIndex;

            public SelectionChangedArgs(T selected, int selectedIndex)
            {
                Selected = selected;
                SelectedIndex = selectedIndex;
            }
        } 

        /// <summary>
        /// The current selection.
        /// This property is synchronized.
        /// </summary>
        public T Selected
        {
            get { return _Selected; }
            set
            {
                _Selected = value;
                _SelectedIndex = Options.IndexOf(value);
                SelectionChanged?.Invoke(this, new SelectionChangedArgs(Selected, SelectedIndex));
            }
        }
        private T _Selected;

        /// <summary>
        /// The index of the current selection.
        /// This property is synchronized, too.
        /// </summary>
        public int SelectedIndex
        {
            get { return _SelectedIndex; }
            set { _SelectedIndex = value; Selected = Options[value]; }
        }
        private int _SelectedIndex;

        /// <summary>
        /// A list of all options to choose from.
        /// </summary>
        public readonly List<T> Options;

        /// <summary>
        /// Invoked when the element was changed.
        /// </summary>
        public event EventHandler<SelectionChangedArgs> SelectionChanged;

        /// <summary>
        /// Creates a new bindable.
        /// </summary>
        /// <param name="options">options to choose from</param>
        /// <param name="selected">the default selection (has to be one of the options)</param>
        public BindableSelection(IEnumerable<T> options, T selected)
        {
            Options = new List<T>(options);
            Selected = selected;
        }

        /// <summary>
        /// Creates a new bindable.
        /// </summary>
        /// <param name="options">options to choose from</param>
        /// <param name="selectedIndex">the index of the default selection (zero based, has to be lower than the option count)</param>
        public BindableSelection(IEnumerable<T> options, int selectedIndex = 0)
        {
            Options = new List<T>(options);
            SelectedIndex = selectedIndex;
        }

        protected internal override object GetValue()
        {
            return Selected;
        }

        /// <summary>
        /// Forward the selected value.
        /// </summary>
        /// <returns>selected value as string</returns>
        public override string ToString()
        {
            return Selected.ToString();
        }
    }   
}
