﻿using System;
using PortableRazor.Json;
using PortableRazor.Web.Html;

namespace PortableRazor.Web.Binding
{
    public interface IReadonlyBindable<T> : IDisposable
    {
        /// <summary>
        /// The value that will be synchronized.
        /// </summary>
        T Value { get; }

        /// <summary>
        /// Register an action that is invoked when the value of this bindable changed.
        /// </summary>
        /// <param name="action">the action to perform</param>
        HtmlString OnChange(Action<T> action);
    }


    /// <summary>
    /// Enables Two-Way Binding between the C# model and certain user controls.
    /// </summary>
    /// <typeparam name="T">the type of the value</typeparam>
    public class Bindable<T> :  BindableValueBase, IReadonlyBindable<T>
    {
        /// <summary>
        /// The value that will be synchronized
        /// </summary>
        public T Value
        {
            get { return _Value; }
            set
            {
                if (_Value != null && _Value.Equals(value)) return; // reduce event overhead if possible.. therefore we have some boxing
                _Value = value;
                InvokeValueChanged();
                _Change?.Invoke(_Value);
            }
        }
        private T _Value;

        /// <summary>
        /// Try to parse a string to set the value of the bindable.
        /// If this failes, the value does not change.
        /// </summary>
        /// <param name="value">text to be parsed</param>
        /// <returns>true i</returns>
        public bool TryParse(string value)
        {
            T parsed;
            if (value.TryParse(out parsed))
            {
                Value = parsed;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Event (actually an action, because we don't need sender information) that can be registered from Razor.
        /// </summary>
        private event Action<T> _Change;

        /// <summary>
        /// Creates a new bindable property.
        /// </summary>
        public Bindable()
        {
            Value = default(T);
        }

        /// <summary>
        /// Creates a new bindable property with a default value.
        /// </summary>
        /// <param name="initialValue"></param>
        public Bindable(T initialValue)
        {
            Value = initialValue;
        }


        /// <summary>
        /// Register an action that is invoked when the value of this bindable changed.
        /// </summary>
        /// <param name="action">the action to perform</param>
        public HtmlString OnChange(Action<T> action)
        {
            _Change += action;
            return String.Empty;
        }

        protected internal override object GetValue()
        {
            return Value;
        }
        
        /// <summary>
        /// Forward the bound value.
        /// </summary>
        /// <returns>value as string</returns>
        public override string ToString()
        {
            if (Value == null)
                return "<null>";

            return Value.ToString();
        }
    }
}
