﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace PortableRazor.Web.Binding
{
    public class BindableList<T> : IDisposable
    {
        public class ListItem : Bindable<T>
        {
            /// <summary>
            /// Index of the item in the list it belongs to.
            /// </summary>
            public IReadonlyBindable<int> Index => _Index;
            private readonly Bindable<int> _Index;

            /// <summary>
            /// Bindable that indicates if this item is the first in the list.
            /// </summary>
            public readonly BindableComputed<bool> IsFirst;

            /// <summary>
            /// Bindable that indicates if this item is the last in the list.
            /// </summary>
            public readonly BindableComputed<bool> IsLast;

            /// <summary>
            /// List this item belongs to.
            /// </summary>
            public BindableList<T> List { get; private set; }

            /// <summary>
            /// Hidden constructor to be called by the list.
            /// </summary>
            /// <param name="list"></param>
            /// <param name="value"></param>
            /// <param name="index"></param>
            internal ListItem(BindableList<T> list, T value) : base(value)
            {
                List = list;
                _Index = new Bindable<int>();
                IsFirst = new BindableComputed<bool>(()=> _Index.Value == 0, _Index);
                IsLast = new BindableComputed<bool>(()=> _Index.Value == (List.Count - 1), _Index);
            }

            /// <summary>
            /// To be called by the list.
            /// </summary>
            internal void SetIndex(int index)
            {
                _Index.Value = index;
                var notAwaited = IsFirst.EvaluateAsync();
                var alsoNotAwaited = IsLast.EvaluateAsync();
            }

            /// <summary>
            /// Destructor is called when the list is disposed or the element is removed.
            /// </summary>
            public override void Dispose()
            {
                Index.Dispose();
                IsFirst.Dispose();
                IsLast.Dispose();
                List = null;
                base.Dispose();
            }

            public override string ToString()
            {
                return $"[{Index.Value}] {Value}";
            }

            /// <summary>
            /// Removes this item from the list.
            /// </summary>
            public void Remove()
            {
                List.Remove(this);
            }

            /// <summary>
            /// Moves this item to a specified position in the list (counting from 0)
            /// </summary>
            /// <param name="toIndex"></param>
            public void Move(int toIndex)
            {
                List.Move(this, toIndex);
            }

            /// <summary>
            /// Get the previous element in the list or null if the current one is the first.
            /// </summary>
            public ListItem Previous => (Index.Value == 0) ? null : List[Index.Value - 1];

            /// <summary>
            /// Get the next element in the list or null if the current one is the last one.
            /// </summary>
            public ListItem Next => (Index.Value == List.Count - 1) ? null : List[Index.Value + 1];
        }

        public class AddedEventArgs : EventArgs
        {
            public readonly ListItem Item;
            public readonly int Index;

            public AddedEventArgs(ListItem item, int index)
            {
                Item = item;
                Index = index;
            }
        }

        public class RemovedEventArgs : EventArgs
        {
            public readonly ListItem Item;
            public readonly int Index;

            public RemovedEventArgs(ListItem item, int index)
            {
                Item = item;
                Index = index;
            }
        }

        public class MovedEventArgs : EventArgs
        {
            public readonly ListItem Item;
            public readonly int FormerIndex;
            public readonly int NewIndex;

            public MovedEventArgs(ListItem item, int formerIndex, int newIndex)
            {
                Item = item;
                FormerIndex = formerIndex;
                NewIndex = newIndex;
            }
        }

        private ObservableCollection<ListItem> _List = new ObservableCollection<ListItem>();

        public event EventHandler<NotifyCollectionChangedEventArgs> ListChanged;

        public event Action<AddedEventArgs> Added; 
        public event Action<RemovedEventArgs> Removed; 
        public event Action<MovedEventArgs> Moved; 


        public BindableList()
        {
            _List.CollectionChanged += CollectionChanged;
        }

        public void Dispose()
        {
            // clear the list
            for (int i = _List.Count - 1; i >= 0; --i)
                Remove(_List[i], true);

            // unhook after clearing
            _List.CollectionChanged -= CollectionChanged;
        }

        private void CollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            System.Diagnostics.Debug.WriteLine($"Collection changed ({args.Action})");
            ListChanged?.Invoke(this, args);

            if (args.Action == NotifyCollectionChangedAction.Add)
            {
                // tell the second last one the it isn't the last one anymore
                if(Count > 1) this[Count - 2].SetIndex(Count - 2);

                // notify other listeners
                Added?.Invoke(new AddedEventArgs((ListItem)args.NewItems[0], args.NewStartingIndex));
            }

            if (args.Action == NotifyCollectionChangedAction.Remove)
            {
                // only one item at the moment
                var item = (ListItem) args.OldItems[0];
                
                // update later indices and the previous in case it's the last now
                for (int i = Math.Max(0, item.Index.Value - 1); i < _List.Count; ++i)
                    _List[i].SetIndex(i);

                // change own index to an invalid value
                item.SetIndex(-1);

                // notify other listeners
                Removed?.Invoke(new RemovedEventArgs(item, args.OldStartingIndex));
            }

            if (args.Action == NotifyCollectionChangedAction.Move)
            {
                // update affected indices 
                for (int i = Math.Min(args.OldStartingIndex, args.NewStartingIndex); i < _List.Count; ++i)
                    _List[i].SetIndex(i);

                // notify other listeners
                Moved?.Invoke(new MovedEventArgs((ListItem)args.OldItems[0], args.OldStartingIndex, args.NewStartingIndex));
            }
        }

        /// <summary>
        /// Gets the number of elements actually contained in the List.
        /// </summary>
        public int Count => _List.Count;
        
        /// <summary>
        /// Get a item based on his index in the list.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public ListItem this[int index] => _List[index];

        /// <summary>
        /// Creates a now ListItem with a specified value.
        /// NOTE: You can only add an item by it's value. This way, it's not possible to remove an item and add it again later.
        /// </summary>
        /// <param name="value">the value of the new list item</param>
        /// <returns>the created list item </returns>
        public ListItem Add(T value)
        {
            var item = new ListItem(this, value);
            _List.Add(item);
            item.SetIndex(_List.Count-1);
            return item;
        }

        /// <summary>
        /// Removes an item from the list.
        /// </summary>
        /// <param name="item">item to remove</param>
        /// <param name="dispose">dispose the removed element [Default: false]</param>
        public void Remove(ListItem item, bool dispose = false)
        {
            _List.Remove(item);
            if(dispose) item.Dispose();
        }

        public void Move(ListItem item, int newIndex)
        {
            if (newIndex == item.Index.Value) return;            // no change
            if(newIndex < 0 || newIndex >= Count) return;      // not possible
            _List.Move(item.Index.Value, newIndex);
        }
    }
}
