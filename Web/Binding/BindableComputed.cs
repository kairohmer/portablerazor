﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortableRazor.Web.Html;

namespace PortableRazor.Web.Binding
{
    public abstract class BindableComputed : BindableValueBase
    {
        public abstract void Unsubcribe(params BindableValueBase[] bindables);
    }

    public class BindableComputed<T> : BindableComputed, IReadonlyBindable<T>
    {
        /// <summary>
        /// The value that will be synchronized
        /// </summary>
        public T Value => _LastComputedValue;

        // required by the by BindableValueBase
        protected internal override object GetValue()
        {
            return _LastComputedValue;
        }

        // keep track of the last
        private T _LastComputedValue;

        /// <summary>
        /// Event (actually an action, because we don't need sender information) that can be registered from Razor.
        /// </summary>
        private event Action<T> _Change;

        /// <summary>
        /// Register an action that is invoked when the value of this bindable changed.
        /// </summary>
        /// <param name="action">the action to perform</param>
        public HtmlString OnChange(Action<T> action)
        {
            _Change += action;
            return String.Empty;
        }


        // atleast one of these two functions is null!
        private Func<Task<T>> _EvaluationFunctionAsync = null;
        private Func<T> _EvaluationFunction = null;

        /// <summary>
        /// Bindables this one observes.
        /// </summary>
        private readonly Dictionary<Guid, BindableValueBase> _Dependencies = new Dictionary<Guid, BindableValueBase>();

        /// <summary>
        /// Creates a new computed bindable property.
        /// </summary>
        public BindableComputed() { }

        /// <summary>
        /// Can be called from Razor and from ViewModel code but it's more usefull in razor code.
        /// Creates a new computed bindable property and sets the action.
        /// In View Model you, you'll get errors, when trying to setup an evaluation function before the ViewModel constructor is called.
        /// Use this default construction, subscribe other bindables, and set action in the ViewModels constructor or during initialization instead.
        /// In Razor code, this is not an issue.
        /// </summary>
        /// <param name="evaluationFunction">the function to evaluate when a subsribed bindable changed.</param>
        /// <param name="dependencies">bindables this one depends on. Everytime they change the evaluation function is executed.</param>
        public BindableComputed(Func<Task<T>> evaluationFunction, params BindableValueBase[] dependencies)
        {
            if(dependencies.Length > 0)
                Subscribe(dependencies);

            SetFunction(evaluationFunction);
        }

        /// <summary>
        /// Can be called from Razor and from ViewModel code but it's more usefull in razor code.
        /// Creates a new computed bindable property and sets the action.
        /// In View Model you, you'll get errors, when trying to setup an evaluation function before the ViewModel constructor is called.
        /// Use this default construction, subscribe other bindables, and set action in the ViewModels constructor or during initialization instead.
        /// In Razor code, this is not an issue.
        /// </summary>
        /// <param name="evaluationFunction">the function to evaluate when a subsribed bindable changed.</param>
        /// <param name="dependencies">bindables this one depends on. Everytime they change the evaluation function is executed.</param>
        public BindableComputed(Func<T> evaluationFunction, params BindableValueBase[] dependencies)
        {
            if (dependencies.Length > 0)
                Subscribe(dependencies);

            SetFunction(evaluationFunction);
        }

        /// <summary>
        /// Set the function to evaluate when a subsribed bindable changed.
        /// The new function will be evaluated immediately.
        /// </summary>
        /// <param name="evaluationFunction">the function to evaluate</param>
        public void SetFunction(Func<Task<T>> evaluationFunction)
        {
            _EvaluationFunctionAsync = evaluationFunction;
            _EvaluationFunction = null;
            var notAwaited = EvaluateAsync(); // invoke evaluation
        }

        /// <summary>
        /// Set the function to evaluate when a subsribed bindable changed. 
        /// The new function will be evaluated immediately.
        /// </summary>
        /// <param name="evaluationFunction">the function to evaluate</param>
        public void SetFunction(Func<T> evaluationFunction)
        {
            _EvaluationFunctionAsync = null;
            _EvaluationFunction = evaluationFunction;
            var notAwaited = EvaluateAsync(); // invoke evaluation
        }

        /// <summary>
        /// Forces the evaluation of the bindables function.
        /// </summary>
        /// <returns></returns>
        public async Task<T> EvaluateAsync()
        {
            // no function set
            if (_EvaluationFunctionAsync == null && _EvaluationFunction == null) return default(T); // TODO notify the developer

            // store the value and start evaluation of the function
            T previousValue = _LastComputedValue;
            _LastComputedValue = _EvaluationFunctionAsync == null ? _EvaluationFunction() : await _EvaluationFunctionAsync();

            if (previousValue == null || !previousValue.Equals(_LastComputedValue))
            {
                // raise change event
                InvokeValueChanged();
                _Change?.Invoke(_LastComputedValue);
            }

            return _LastComputedValue;
        }


        /// <summary>
        /// Register bindables to observe. If one of them changes, the value of this bindable is re-evaluated.
        /// You can call this multiple times. 
        /// Each passed bindable is registered until you call 'Unsubcribe'.
        /// Calling this function with at least one new bindable will invoke the execution of the evaluation function.
        /// </summary>
        /// <param name="bindables">bindables to subsribe to</param>
        public void Subscribe(params BindableValueBase[] bindables)
        {
            bool newDependency = false;

            foreach (BindableValueBase bindable in bindables)
            {
                // already subscribing
                if(_Dependencies.ContainsKey(bindable.Handle)) continue;

                // manage dependencies
                _Dependencies.Add(bindable.Handle, bindable);   // we keep the bindable
                bindable.Subscribers.Add(this.Handle, this);    // we tell the other one that we are intested
                newDependency = true;

                // hook
                bindable.ValueBaseChanged += OnDependencyChanged;
            }

            if (newDependency)
            {
                var notAwaited = EvaluateAsync(); // invoke evaluation
            }
        }

        /// <summary>
        /// Removed currently observed bindables.
        /// </summary>
        /// <param name="bindables">bindables to unsubscribe from</param>
        public override void Unsubcribe(params BindableValueBase[] bindables)
        {
            foreach (BindableValueBase bindable in bindables)
            {
                // already subscribing
                if (!_Dependencies.ContainsKey(bindable.Handle)) continue;

                // manage dependencies
                _Dependencies.Remove(bindable.Handle);
                bindable.Subscribers.Remove(this.Handle);

                // unhook
                bindable.ValueBaseChanged -= OnDependencyChanged;
            }
        }

        /// <summary>
        /// Called when a observed bindable changed.
        /// </summary>
        private async void OnDependencyChanged()
        {
            await EvaluateAsync();
        }
    }
}
