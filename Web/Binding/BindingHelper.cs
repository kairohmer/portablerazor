﻿using System;
using System.Collections.Generic;
using System.IO;
using PortableRazor.Json;
using PortableRazor.Utilities;
using PortableRazor.Utilities.Collections;
using PortableRazor.Web.Base;
using PortableRazor.Web.Html;

namespace PortableRazor.Web.Binding
{
    public enum InputType
    {
        Text,
        Password,
        Search,
        Checkbox
    }

    public partial class BindingHelper : BaseHelper
    {
        /// <summary>
        /// Ever thing that is related to events that can be invoked from DOM elements.
        /// E.g., 'oninput', 'onchange', ... 
        /// </summary>
        public readonly BindingHelperEvents Events;

        private Logging Log;

        /// <summary>
        /// Keep track of bindables that need to be freed with the view or viewmodel.
        /// </summary>
        private readonly List<BindableBase> GeneratedBindables = new List<BindableBase>();
        private readonly Dictionary<Guid, SynchronizedDictionary<Guid, Guid>> BoundViewModelsByForeach = new Dictionary<Guid, SynchronizedDictionary<Guid, Guid>>();


        public BindingHelper(PortableRazorContext context, RazorRenderable host) : base(context, host)
        {
            Events = new BindingHelperEvents(this);
            Log = new Logging(GetType().Name);
        }

        public override void Dispose()
        {
            for (int i = GeneratedBindables.Count - 1; i >= 0; --i)
            {
                var bindable = GeneratedBindables[i];
                GeneratedBindables.RemoveAt(i);
                bindable.Dispose();
            }

            // free all items of dynamic lists
            foreach (var list in BoundViewModelsByForeach.Values)
            {
                foreach (var item in list.Values)
                {
                    var viewModel = RegisteredObject.GetByGuid(item);
                    viewModel.Dispose();
                }
            }
        }



        /// <summary>
        /// Creates a new id object to select certain DOM elements for bindings.
        /// </summary>
        /// <returns></returns>
        public BindingId GenerateId()
        {
            BindingId id = new BindingId(Context);
            GeneratedBindables.Add(id);
            return id;
        }
    }
}
