﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PortableRazor.Json;
using PortableRazor.Utilities;
using PortableRazor.Utilities.Collections;
using PortableRazor.Web.Base;
using PortableRazor.Web.Html;

namespace PortableRazor.Web.Binding
{
    /// <summary>
    /// Coupling from the perspective of the DOM element.
    /// </summary>
    public enum BindingMode
    {
        /// <summary>
        /// Dom element and bindable are synchronized.
        /// Changes on one are reflected to the other.
        /// </summary>
        TwoWay,

        /// <summary>
        /// Everytime the bindable changes, the DOM element is updated.
        /// The DOM element only reads the value.
        /// </summary>
        Read,

        /// <summary>
        /// Everytime the value of the DOM element changes, the bindable is updated.
        /// The DOM element only writes the value.
        /// </summary>
        Write,

        /// <summary>
        /// Special mode without synchronization.
        /// This can be used if changes on DOM elements need to be passed to the ViewModel code without synchronization.
        /// To do this:
        ///  - create an 'InputBindingId' in the ViewModel:     InputBindingId SomeID = Binding.GenerateInputBindingId();
        ///  - hock to the Input event:                         SomeID.Input += ...
        ///  - pass during the setup of the Bind;               <input (at)Binding.Input(id, BindingMode.None) />
        /// </summary>
        None,
    }

    public partial class BindingHelper : BaseHelper
    {

        /// <summary>
        /// For synchronizing bindables with <input/> elements.
        /// CAUTION: this call will set the 'id', 'oninput' and 'value' attribute of the element. Do not set them a second time! 
        /// </summary>
        /// <typeparam name="T">type of bindables value</typeparam>
        /// <param name="bindable">the bindable to synchronize with</param>
        /// <param name="mode">two-way coupling or one direction only</param>
        /// <returns>renderable html text</returns>
        public HtmlString Input<T>(Bindable<T> bindable, BindingMode mode = BindingMode.TwoWay)
        {
            var id = GenerateId();
            return Input(id, bindable, mode);
        }

        /// <summary>
        /// For synchronizing bindables with <input/> elements.
        /// CAUTION: this call will set the 'id', 'oninput' and 'value' attribute of the element. Do not set them a second time! 
        /// </summary>
        /// <typeparam name="T">type of bindables value</typeparam>
        /// <param name="id">use a specified id (maybe one specified in the ViewModel code) CAUTION: do not assign the ID to another DOM element!</param>
        /// <param name="bindable">the bindable to synchronize with</param>
        /// <param name="mode">two-way coupling or one direction only</param>
        /// <returns>renderable html text</returns>
        public HtmlString Input<T>(BindingId id, Bindable<T> bindable, BindingMode mode = BindingMode.TwoWay)
        {
            string readHtml = "";
            string writeHtml = "";

            // reading enabled
            if (mode == BindingMode.TwoWay || mode == BindingMode.Read)
            {
                bindable.OnChange(async (value) => await id.SetValueAsync($"{value}"));
                readHtml = $" value=\"{bindable.Value}\"";
                Host.RazorWriter.LoadScripts += async () => await id.SetValueAsync(bindable.Value.ToString()); // init value when the page is shown
            }

            // writing enabled
            if (mode == BindingMode.TwoWay || mode == BindingMode.Write)
            {
                writeHtml = $" oninput=\"{Events.OnInput(id, async (e) => bindable.TryParse(await e.Id.GetValueAsync()))}\"";
            }

            // combine rendered html
            return $"id=\"{id}\"{readHtml}{writeHtml}";
        }

        /// <summary>
        /// For synchronizing bindables with <span></span> elements.
        /// CAUTION: this call will set the 'id' attribute of the element. Do not set it a second time! 
        /// </summary>
        /// <typeparam name="T">type of bindables value</typeparam>
        /// <param name="bindable">the bindable to synchronize with</param>
        /// <param name="formatString">custom format to specify decimal digits for example</param>
        /// <returns>renderable html text</returns>
        public HtmlString Span<T>(IReadonlyBindable<T> bindable, string formatString = "{0}")
        {
            var id = GenerateId();
            return Span(id, bindable, formatString);
        }

        /// <summary>
        /// For synchronizing bindables with <span></span> elements.
        /// CAUTION: this call will set the 'id' attribute of the element. Do not set it a second time! 
        /// </summary>
        /// <typeparam name="T">type of bindables value</typeparam>
        /// <param name="id">use a specified id (maybe one specified in the ViewModel code) CAUTION: do not assign the ID to another DOM element!</param>
        /// <param name="bindable">the bindable to synchronize with</param>
        /// <param name="formatString">custom format to specify decimal digits for example</param>
        /// <returns>renderable html text</returns>
        public HtmlString Span<T>(BindingId id, IReadonlyBindable<T> bindable, string formatString = "{0}")
        {
            bindable.OnChange(async (value) => await id.SetTextAsync(string.Format(formatString, value)));
            Host.RazorWriter.LoadScripts += async () => await id.SetTextAsync(string.Format(formatString, bindable.Value));
            return $"id=\"{id}\"";
        }


        /// <summary>
        /// For synchronizing bindables with <input type="checkbox"/> elements.
        /// CAUTION: this call will set the 'id', 'onchange' and 'value' attribute of the element. Do not set them a second time! 
        /// </summary>
        /// <param name="bindable">the bindable to synchronize with</param>
        /// <param name="mode">two-way coupling or one direction only</param>
        /// <returns>renderable html text</returns>
        public HtmlString Checkbox(Bindable<bool> bindable, BindingMode mode = BindingMode.TwoWay)
        {
            var id = GenerateId();
            return Checkbox(id, bindable, mode);
        }

        /// <summary>
        /// For synchronizing bindables with <input type="checkbox"/> elements.
        /// CAUTION: this call will set the 'id', 'onchange' and 'value' attribute of the element. Do not set them a second time! 
        /// </summary>
        /// <param name="id">use a specified id (maybe one specified in the ViewModel code) CAUTION: do not assign the ID to another DOM element!</param>
        /// <param name="bindable">the bindable to synchronize with</param>
        /// <param name="mode">two-way coupling or one direction only</param>
        /// <returns>renderable html text</returns>
        public HtmlString Checkbox(BindingId id, Bindable<bool> bindable, BindingMode mode = BindingMode.TwoWay)
        {
            string readHtml = "";
            string writeHtml = "";

            // reading enabled
            if (mode == BindingMode.TwoWay || mode == BindingMode.Read)
            {
                bindable.OnChange(async (value) => await id.SetCheckedAsync(value));
                Host.RazorWriter.LoadScripts += async () => await id.SetCheckedAsync(bindable.Value); // init value when the page is shown
            }

            // writing enabled
            if (mode == BindingMode.TwoWay || mode == BindingMode.Write)
            {
                writeHtml = $" onchange=\"{Events.OnChange(id, async (e) => bindable.Value = await e.Id.GetCheckedAsync())}\"";
            }

            // combine rendered html
            return $"id=\"{id}\"{readHtml}{writeHtml}";
        }


        /// <summary>
        /// Synchronizes the value of bindable<bool> with the visibility of an DOM element.
        /// You can also invert the visibility to show the element if the boolean is false.
        /// </summary>
        /// <param name="id">use a specified id (maybe one specified in the ViewModel code) CAUTION: do not assign the ID to another DOM element!</param>
        /// <param name="bindable">the bindable to synchronize with</param>
        /// <param name="inverse">if true, the element will be visible only if the value of the bindable is false</param>
        /// <returns></returns>
        public HtmlString Visibility(BindingId id, IReadonlyBindable<bool> bindable, bool inverse = false)
        {
            // local function
            async void OnChangeFunc(bool visible)
            {
                if (inverse) visible = !visible;
                await id.SetVisibility(visible ? ElementVisibility.Visible : ElementVisibility.Hidden);
            }

            bindable.OnChange(OnChangeFunc);
            Host.RazorWriter.LoadScripts += () => OnChangeFunc(bindable.Value);
            return "";
        }

        public class DynamicForeachToken<T>
        {
            private readonly BindableList<T> _DynamicList;
            private readonly BindingHelper _Binding;
            private readonly BindingId _Id;

            public override string ToString()
            {
                return _Binding.Foreach<DefaultListItemTemplate<T>, T>(_Id, _DynamicList);
            }

            public DynamicForeachToken(BindingHelper binding, BindingId id, BindableList<T> dynamicList)
            {
                _Binding = binding;
                _Id = id;
                _DynamicList = dynamicList;
            }

            public HtmlString UseTemplate<TListItem>() where TListItem : ViewModelListItemTemplate<T>
            {
                return _Binding.Foreach<TListItem, T>(_Id, _DynamicList);
            }

            public HtmlString UseTemplate(Func<BindableList<T>.ListItem, HtmlString> inlineTemplate)
            {
                return _Binding.Foreach<InlineListItemTemplate<T>, T>(_Id, _DynamicList, inlineTemplate);
            }
        }

        public class StaticForeachToken<T>
        {
            private readonly IEnumerable<T> _StaticList;
            private readonly BindingHelper _Binding;
            private readonly BindingId _Id;

            public override string ToString()
            {
                return _Binding.Foreach<DefaultItemTemplate<T>, T>(_Id, _StaticList);
            }

            public StaticForeachToken(BindingHelper binding, BindingId id, IEnumerable<T> staticList)
            {
                _Binding = binding;
                _Id = id;
                _StaticList = staticList;
            }

            public HtmlString UseTemplate<TListItem>() where TListItem : ViewModelItemTemplate<T>
            {
                return _Binding.Foreach<TListItem, T>(_Id, _StaticList);
            }

            public HtmlString UseTemplate(Func<T, HtmlString> inlineTemplate)
            {
                return _Binding.Foreach<InlineItemTemplate<T>, T>(_Id, _StaticList, inlineTemplate);
            }
        }

        public StaticForeachToken<T> Foreach<T>(IEnumerable<T> staticList)
        {
            var id = GenerateId();
            return new StaticForeachToken<T>(this, id, staticList);
        }

        public StaticForeachToken<T> Foreach<T>(BindableSelection<T> selection)
        {
            var id = GenerateId();
            return new StaticForeachToken<T>(this, id, selection.Options);
        }

        public DynamicForeachToken<T> Foreach<T>(BindableList<T> dynamicList)
        {
            var id = GenerateId();
            return new DynamicForeachToken<T>(this, id, dynamicList);
        }

        public DynamicForeachToken<T> Foreach<T>(BindingId id, BindableList<T> dynamicList)
        {
            return new DynamicForeachToken<T>(this, id, dynamicList);
        }

        private HtmlString Foreach<TListItem, T>(BindingId id, IEnumerable<T> list, Func<T, HtmlString> inlineTemplate = null) where TListItem : ViewModelItemTemplate<T>
        {
            BoundViewModelsByForeach.Add(id.Handle, new SynchronizedDictionary<Guid, Guid>(32));

            // init the dynamicList with the elements already in the dynamicList    
            Host.RazorWriter.LoadScripts += async () =>
            {
                foreach (var added in list)
                {
                    // create template
                    ViewModelItemTemplate<T> viewModel = null;
                    if (typeof(TListItem) == typeof(InlineItemTemplate<T>))
                    {
                        InlineItemTemplate<T> viewModelInlineTemplate = ViewModelTemplate.CreateViewModelForItem<InlineItemTemplate<T>, T>(Context);
                        viewModelInlineTemplate.Format = inlineTemplate;
                        viewModel = viewModelInlineTemplate;
                    }
                    else
                    {
                        viewModel = ViewModelTemplate.CreateViewModelForItem<TListItem, T>(Context);
                    }

                    viewModel.Item = added;

                    // add the new view model reference
                    if (!BoundViewModelsByForeach[id.Handle].TryAdd(viewModel.Handle, viewModel.Handle)) // store under own handle
                    {
                        Log.Error($"[Foreach] Failed to add List Item: {added}");
                    }

                    // render template
                    var writer = await viewModel.GenerateAsync();
                    await Context.WebView.EvaluateJavascriptAsync(
                        $"var item = $.parseHTML('{HtmlHelper.EscapeHtmlString(writer.Main.ToString())}');  \n" +   // create item
                        $"$(item).prop('id','vm_{viewModel.Handle}');                                       \n" +   // set the id to view model handle
                        $"$('#{id}').append(item);                                                          \n"     // add the item to the DOM
                        );
                    writer.RunLoadScripts();
                }
            };

            return $"id='{id}'";
        }


        private HtmlString Foreach<TListItem, T>(BindingId id, BindableList<T> list, Func<BindableList<T>.ListItem, HtmlString> inlineTemplate = null) where TListItem : ViewModelListItemTemplate<T>
        {
            
            BoundViewModelsByForeach.Add(id.Handle, new SynchronizedDictionary<Guid, Guid>(32));

            // add item at the end of the dynamicList
            Func<BindableList<T>.ListItem, Task> addAction = async (added) =>
            {
                // create template
                ViewModelListItemTemplate<T> viewModel = null;
                if (typeof(TListItem) == typeof(InlineListItemTemplate<T>))
                {
                    InlineListItemTemplate<T> viewModelInlineTemplate = ViewModelTemplate.CreateViewModelForListItem<InlineListItemTemplate<T>, T>(Context);
                    viewModelInlineTemplate.Format = inlineTemplate;
                    viewModel = viewModelInlineTemplate;
                }
                else
                {
                    viewModel = ViewModelTemplate.CreateViewModelForListItem<TListItem, T>(Context);
                }

                viewModel.Item = added;

                // add the new view model reference
                BoundViewModelsByForeach[id.Handle].TryAdd(added.Handle, viewModel.Handle);

                // render template
                var writer = await viewModel.GenerateAsync();
                await Context.WebView.EvaluateJavascriptAsync(
                        $"var item = $.parseHTML('{HtmlHelper.EscapeHtmlString(writer.Main.ToString())}');  \n" +   // create item
                        $"$(item).prop('id','vm_{viewModel.Handle}');                                       \n" +   // set the id to view model handle
                        $"$('#{id}').append(item);                                                          \n"     // add the item to the DOM
                    );
                writer.RunLoadScripts();
            };

            // remove a selected item from the dynamicList
            Func<BindableList<T>.ListItem, Task> removeAction = async (removed) =>
            {
                Guid viewModelHandle;
                if (BoundViewModelsByForeach[id.Handle].TryRemove(removed.Handle, out viewModelHandle)) // get the view models of this item
                {
                    var viewModel = RegisteredObject.GetByGuid<TListItem>(viewModelHandle);

                    // remove from DOM
                    await Context.WebView.EvaluateJavascriptAsync($"$('#vm_{viewModel.Handle}').remove()");
                    viewModel.Dispose();
                }
            };

            Func<BindableList<T>.ListItem, Task> movedAction = async (moved) =>
            {
                Guid movedViewModelHandle;
                if(BoundViewModelsByForeach[id.Handle].TryGet(moved.Handle, out movedViewModelHandle))
                {
                    var movedViewModel = RegisteredObject.GetByGuid<TListItem>(movedViewModelHandle);

                    // move in front of the new successor
                    if (moved.Index.Value == 0)
                    {
                        Guid nextViewModelHandle;
                        if (BoundViewModelsByForeach[id.Handle].TryGet(moved.Next.Handle, out nextViewModelHandle))
                        {
                            var nextViewModel = RegisteredObject.GetByGuid<TListItem>(nextViewModelHandle);

                            // move in DOM
                            await Context.WebView.EvaluateJavascriptAsync(
                                $"var item = $('#vm_{movedViewModel.Handle}').detach();     \n" +   // detach from the DOM
                                $"item.insertBefore($('#vm_{nextViewModel.Handle}'));      \n"     // reattach it to the DOM
                            );
                        }
                    }
                    // move behind the new predecessor
                    else
                    {
                        Guid prevViewModelHandle;
                        if (BoundViewModelsByForeach[id.Handle].TryGet(moved.Previous.Handle, out prevViewModelHandle))
                        {
                            var prevViewModel = RegisteredObject.GetByGuid<TListItem>(prevViewModelHandle);

                            // move in DOM
                            await Context.WebView.EvaluateJavascriptAsync(
                                $"var item = $('#vm_{movedViewModel.Handle}').detach();     \n" +   // detach from the DOM
                                $"item.insertAfter($('#vm_{prevViewModel.Handle}'));       \n"     // reattach it to the DOM
                            );
                        }
                    }
                }
            };

            // listen for changes
            list.Added += async (e) => await addAction(e.Item);
            list.Removed += async (e) => await removeAction(e.Item);
            list.Moved += async (e) => await movedAction(e.Item);

            // init the dynamicList with the elements already in the dynamicList    
            Host.RazorWriter.LoadScripts += async () =>
            {
                for (int i = 0; i < list.Count; ++i)
                    await addAction(list[i]);
            };

            return $"id='{id}'";
        }

        // -- old stuff to remove -----------------------------------------------------------------

        /*
        public HtmlString Select(BindableSelection data, string name = null, object htmlAttributes = null)
        {
            name = String.IsNullOrEmpty(name) ? "handle_" + data.Handle : name;
            string id = GenerateId(data);

            // send changes to the C# bindable
            string onchange = $"window.external.notify('razorbinding:' + encodeURI(JSON.stringify(createHandleValuePair('{data.Handle}', this.value))))";

            // receive changes (from C#, other bindings and unfortunately even if the own input)
            data.RegisterListener(Context, async (BindableBase s) =>
            {
                await Context.WebView.EvaluateJavascriptAsync($"$('#{id}').val('{((BindableSelection)s).Selected}')");
            });

            // build control with options
            string elementString = $"<select name=\"{name}\" id=\"{id}\" onchange=\"{onchange}\" {GenerateHtmlAttributes(htmlAttributes)}>\n";
            for(int i=0; i<data.Options.Count; ++i)
                elementString += $"<option{(data.SelectedIndex == i ? " selected='selected'" : "")}>{data.Options[i]}</option>";
            elementString += "</select>";

            return new HtmlString(elementString);

        }*/
        
    }
}
