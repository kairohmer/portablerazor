﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PortableRazor.Web.Binding
{
    public abstract class BindableValueBase : BindableBase
    {
        /// <summary>
        /// Needs to implemented by deriving classes as this is a crucial part of the binding.
        /// </summary>
        /// <returns>the value of the bindable</returns>
        protected internal abstract object GetValue();

        /// <summary>
        /// Used for Computed Bindables.
        /// Event to hook and get informed about changes.
        /// </summary>
        protected internal event Action ValueBaseChanged;

        /// <summary>
        /// Has to be called everytime a deriving class instance changes it's value.
        /// </summary>
        protected void InvokeValueChanged() => ValueBaseChanged?.Invoke();

        /// <summary>
        /// Used for Computed Bindables.
        /// Contains all the binables that depend on the current one.
        /// </summary>
        protected internal readonly Dictionary<Guid, BindableComputed> Subscribers = new Dictionary<Guid, BindableComputed>();

        /// <summary>
        /// Clean up.
        /// Notifiy all computed bindables that depend on this bindable, that this bindble is not longer available.
        /// </summary>
        public override void Dispose()
        {
            // looks a bit stange this way
            // but Unsubscribe will actually change the dictionary 
            while (Subscribers.Count > 0)
            {
                var pair = Subscribers.First();
                pair.Value.Unsubcribe(this);
            }

            base.Dispose();
        }
    }
}
