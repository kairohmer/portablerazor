﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortableRazor.Web.Html;

namespace PortableRazor.Web.Binding
{
    /// <summary>
    /// to keep things logically separate. All the things related to events from DOM elements are handled by this class.
    /// </summary>
    public class BindingHelperEvents : IDisposable
    {
        public BindingHelper Binding { get; private set; }

        internal BindingHelperEvents(BindingHelper parentHelper)
        {
            Binding = parentHelper;
        }

        public void Dispose()
        {
            Binding = null;
        }


        /// <summary>
        /// Creates an anonymous event handler.
        /// Use the overloaded method to register the handler of a specific InputBindingId instance.
        /// CAUTION: This call will set the 'id' attribute of the element. Do not set it a second time! 
        /// Usage: <input oninput="(at)Binding.OnInput((e) => {...})" />
        /// </summary>
        /// <param name="action">the action to perform when this event happens</param>
        /// <returns>html string to render</returns>
        public HtmlString OnInput(Action<BindingEventArgs> action)
        {
            var id = Binding.GenerateId();
            return Utilities.JavaScriptUtilities.WrapToInlineFunction(
                       $"$(this).prop('id','{id}');" +
                       OnInput(id, action)
                   );
        }

        /// <summary>
        /// Creates an event handler for a specified InputBindingId instance.
        /// CAUTION: This assumes you already set the the passed BindingId-Object to the 'id' attribute of the element. 
        /// Usage: <input oninput="(at)Binding.OnInput(id, (e) => {...})" />
        /// </summary>
        /// <param name="id">the instance to use</param>
        /// <param name="action">the action to perform when this event happens</param>
        /// <returns>html string to render</returns>
        public HtmlString OnInput(BindingId id, Action<BindingEventArgs> action)
        {
            id.Input += action;
            return GenerateJavascriptEventHandler(id, "oninput");
        }

        /// <summary>
        /// Creates an anonymous event handler.
        /// Use the overloaded method to register the handler of a specific InputBindingId instance.
        /// CAUTION: This call will set the 'id' attribute of the element. Do not set it a second time! 
        /// Usage: <checkbox onchange="(at)Binding.OnChange((e) => {...})" />
        /// </summary>
        /// <param name="action">the action to perform when this event happens</param>
        /// <returns>html string to render</returns>
        public HtmlString OnChange(Action<BindingEventArgs> action)
        {
            var id = Binding.GenerateId();
            return Utilities.JavaScriptUtilities.WrapToInlineFunction(
                        $"$(this).prop('id','{id}');" +
                        OnChange(id, action)
                    );
        }

        /// <summary>
        /// Creates an event handler for a specified InputBindingId instance.
        /// CAUTION: This assumes you already set the the passed BindingId-Object to the 'id' attribute of the element. 
        /// Usage: <checkbox onchange="(at)Binding.OnChange((e) => {...})" />
        /// </summary>
        /// <param name="id">the instance to use</param>
        /// <param name="action">the action to perform when this event happens</param>
        /// <returns>html string to render</returns>
        public HtmlString OnChange(BindingId id, Action<BindingEventArgs> action)
        {
            id.Change += action;
            return GenerateJavascriptEventHandler(id, "onchange");
        }



        public HtmlString OnClick(BindingId id, Action<BindingEventArgs> action)
        {
            id.Click += action;
            return GenerateJavascriptEventHandler(id, "onclick");
        }

        public HtmlString OnClick(Action<BindingEventArgs> action)
        {
            var id = Binding.GenerateId();
            return Utilities.JavaScriptUtilities.WrapToInlineFunction(
                        $"$(this).prop('id','{id}');" +
                        OnClick(id, action)
                    );
        }

        public HtmlString OnClick(Action action)
        {
            return OnClick((e) => action());
        }

        public HtmlString OnClick(BindingId id, Action action)
        {
            return OnClick(id, (e) => action());
        }


        private HtmlString GenerateJavascriptEventHandler(BindingId id, string eventName)
        {
            return $"window.external.notify('razorbindingevent:' + encodeURI(JSON.stringify(createHandleValuePair('{id.Handle}', '{eventName}'))))";
        }

    }
}
