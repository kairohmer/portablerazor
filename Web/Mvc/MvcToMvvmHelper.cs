﻿using System;
using System.IO;
using PortableRazor.Web.Base;
using PortableRazor.Web.Html;
using PortableRazor.Web.Mvvm;

namespace PortableRazor.Web.Mvc
{
    /// <summary>
    /// Interface class to allow Mvc bases Programs to use insert MVVM Elements.
    /// This allows mixture Models and offers flexibility.
    /// But the most important reason is that purely MVVM based applications can be hard to setup, especially if remote logins and so on are required.
    /// Therefore, we will usually use the Mvc pattern for app startup and higher level navigation and embedd MVVM to realize the more specific tasks.
    /// This class allows this kind of embedding.
    /// </summary>
    public class MvcToMvvmHelper : BaseHelper
    {
        private readonly NavigationHelper _NavigationHelper;
        
        public MvcToMvvmHelper(PortableRazorContext context, RazorRenderable host) : base(context, host)
        {
            _NavigationHelper = new NavigationHelper(context, host);
        }

        public override void Dispose()
        {
            _NavigationHelper.Dispose();
        }

        //---------------------------------------------------------------------
        // Forward Renderers (allows feature selection)
        //---------------------------------------------------------------------

        /// <summary>
        /// Creates a ViewModelSlot to bind a MVVM ViewModel.
        /// </summary>
        /// <param name="slot"></param>
        /// <returns></returns>
        public HtmlString Slot(MvcToMvvmBindableViewModelSlot slot)
        {
            slot.Bind(_NavigationHelper);
            return _NavigationHelper.Slot(slot); 
        }
    }

    /// <summary>
    /// Special Navigation slot, that can also be used from Mvc to create a bridge to MVVM.
    /// </summary>
    public class MvcToMvvmBindableViewModelSlot : BindableViewModelSlot
    {
        //---------------------------------------------------------------------
        // Forward Renderers (allows feature selection)
        //---------------------------------------------------------------------

        /// <summary>
        /// --- Controller-Code and RAZOR (limited*) ---
        /// Navigate to another Viewmodel and replace the current one.
        /// The returned value can be manipulated in a pipeline style, e.g.: NoHistory() ..
        /// To start (or finish the description of) the actual navigation call "Go()" at the end of the pipeline. 
        /// 
        /// * The "With()" Extensions is not supported at the moment. So to pass parameters, you need to navigate from Controller-side.
        /// </summary>
        /// <typeparam name="TView">the view model type (ATTENTION: this has to be a Type generated from .cshtml</typeparam>
        /// <returns>a navigation token, that can be altered in a pipeline fashion. In the end, call Go() to start finish the navigation</returns>
        public NavigationHelper.NavigationToken<TView> NavigateTo<TView>() where TView : ViewModelBase, new()
        {
            return Navigation.To<TView>().In(this);
        }

        /// <summary>
        /// --- RAZOR and Controller-Code ---
        /// If there view models in the navigation history, you can go back by calling this function.
        /// </summary>
        /// <returns>true if it's possible</returns>
        public bool CanGoBack()
        {
            return Navigation.CanGoBack();
        }

        /// <summary>
        /// --- RAZOR and Controller-Code ---
        /// If there view models in the navigation history, you can go back by calling this function.
        /// </summary>
        /// <returns>Renderable String (RAZOR only)</returns>
        public HtmlString Back()
        {
            return Navigation.Back();
        }
    }
}