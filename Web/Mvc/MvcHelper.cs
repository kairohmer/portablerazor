﻿using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using Newtonsoft.Json;
using PortableRazor.Web.Base;
using PortableRazor.Web.Html;

namespace PortableRazor.Web.Mvc
{
	public class MvcHelper : BaseHelper
    {
	    public MvcHelper(PortableRazorContext context, RazorRenderable host) : base(context, host)
	    {
	    }

        public override void Dispose()
        {
        }


	    public enum FormMethod { Get, Post };

        public HtmlString CallController(string methodName, dynamic args = null)
        {
            dynamic callBag = new
            {
                Method = methodName,
                Arguments = args,
            };

            string serialized = JsonConvert.SerializeObject(callBag);
            serialized = WebUtility.UrlEncode(serialized);

            return $"window.external.notify('razorcall:' + '{serialized}')";
        }

        public HtmlString NavigateBack()
        {
            return CallController("NavigateBack");
        }

        public OpenElement BeginForm(string actionName = "", string controllerName = "", object routeValues = null, FormMethod method = FormMethod.Get, object htmlAttributes = null)
        {

            var form = String.Format("<form action=\"{0}{1}{2}{3}\" method=\"{4}\"{5}>",
                Context.UrlScheme,
                String.IsNullOrEmpty(controllerName) ? String.Empty : controllerName + "/",
                actionName,
                Utilities.HtmlUtilities.GenerateQueryString(routeValues),
                method == FormMethod.Post ? "post" : "get",
                GenerateHtmlAttributes(htmlAttributes));
            Host.RazorWriter.Main.Write(form);

            return new OpenElement(Host.RazorWriter, "form");
        }

        /// <summary>
        /// Creates the href property of a link.
        /// </summary>
        /// <param name="actionName">name of the action call(controller method name)</param>
        /// <param name="controllerName">name of the controller to call</param>
        /// <param name="routeValues">query string (e.g.: ?foo=bar&blub=42)</param>
        /// <returns></returns>
        public HtmlString Action(string actionName, string controllerName, object routeValues = null)
        {
            return string.Format("{0}{1}{2}{3}",
                Context.UrlScheme,
                String.IsNullOrEmpty(controllerName) ? String.Empty : controllerName + "/",
                actionName,
                Utilities.HtmlUtilities.GenerateQueryString(routeValues));
        }

        /// <summary>
        /// creates a link (not just the href like @Mvc.Action does)
        /// </summary>
        /// <param name="linkText">link text shown to the user</param>
        /// <param name="actionName">name of the action call(controller method name)</param>
        /// <param name="controllerName">name of the controller to call</param>
        /// <param name="routeValues">query string (e.g.: ?foo=bar&blub=42)</param>
        /// <param name="htmlAttributes">additional html properties like class or style ([usage: new {Class = "foo"}]</param>
        /// <returns></returns>
        public HtmlString ActionLink(string linkText, string actionName, string controllerName, object routeValues = null, object htmlAttributes = null)
        {
            return new HtmlString(string.Format("<a href=\"{0}{1}{2}{3}\"{4}>{5}</a>",
                Context.UrlScheme,
                string.IsNullOrEmpty(controllerName) ? String.Empty : controllerName + "/",
                string.IsNullOrEmpty(actionName) ? "Index" : actionName,
                Utilities.HtmlUtilities.GenerateQueryString(routeValues),
                GenerateHtmlAttributes(htmlAttributes),
                linkText));
        }
    }
}

