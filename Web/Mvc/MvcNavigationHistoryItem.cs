﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortableRazor.Web.Mvc
{
    public class MvcNavigationHistoryItem : NavigationHistoryItem
    {
        private readonly MvcContext m_Context;
        private readonly string m_Url;

        /// <summary>
        /// Called by the MvcContext.
        /// </summary>
        /// <param name="context">the context</param>
        /// <param name="url">the url to navigate to reach the view</param>
        internal MvcNavigationHistoryItem(MvcContext context, string url)
        {
            m_Context = context;
            m_Url = url;
        }

        public override void Dispose()
        {
            // nothing to clean up here
        }

        protected override async Task<bool> NavigatedBackAsync()
        {
            await Task.WhenAll();
            return m_Context.HandleRequest(m_Url, false); // TODO [NavigatedBackAsync] check if we can do this async 
        }
    }
}
