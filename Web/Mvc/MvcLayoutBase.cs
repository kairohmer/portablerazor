using System;
using PortableRazor.Web.Html;

namespace PortableRazor.Web.Mvc
{
	public abstract class MvcLayoutBase : MvcViewBase
	{
	    private MvcViewBase _BodyView;

	    internal void InitializeLayout(MvcViewBase body)
	    {
	        _BodyView = body;
	    }
       
	    public HtmlString RenderBody()
	    {
	        return _BodyView.GenerateString();
	    }
	}
}

