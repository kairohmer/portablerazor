﻿using System;
using System.Collections.Generic;
using System.Reflection;
using PortableRazor.Utilities;
using PortableRazor.Web.Html;

namespace PortableRazor.Web.Mvc
{
	public partial class MvcContext
    {
	    internal class RegisteredController
	    {
	        public Type ControllerType;
	        public ApplicationContext AppContext;
	        public string Name;
	    }

        /// <summary>
        /// Registered controllers the router can direct to.
        /// </summary>
	    private Dictionary<string, RegisteredController> m_RegisteredControllers { get; } = new Dictionary<string, RegisteredController>(StringComparer.OrdinalIgnoreCase);

        /// <summary>
        /// The default controller to use if only an action is provided in the target.
        /// </summary>
	    private RegisteredController m_DefaultController;

        /// <summary>
        /// The currently active controller.
        /// Is required internally, e.g., to forward java script notifications.
        /// This will change while navigating to another router.
        /// </summary>
        internal Controller ActiveController { get; private set; }

        /// <summary>
        /// Register a top level controller for a certain route, e.g. for using the Mvc pattern.
        /// It's possible to add the controller multiple times with different route.
        /// Usually called by the applications.
        /// </summary>
        /// <typeparam name="T">type of controller to register</typeparam>
        /// <param name="appContext">context that is passed to each created controller instance</param>
        /// <param name="routeName">Name that used for routing. If null, the name of the controller is used.</param>
        public void RegisterController<T>(ApplicationContext appContext, string routeName = null) where T : Controller
	    {
	        // default title matches the controller name
	        string name = typeof(T).FullName;

	        // remove the assembly
	        name = name.Substring(name.IndexOf('.') + 1);

	        // name space has to be "<RootNamespace>.Controllers.<ControllerPathWithName>Controller
	        if (!name.StartsWith("Controllers"))
	            throw new Exception("Controllers have to be defined in <RootNamespace>.Controllers");

	        // remove prefix
	        name = name.Substring(12);

	        // check the controller name
	        if (!name.EndsWith("Controller"))
	            throw new Exception("Controller Class name has to end with 'Controller'");

	        // remove suffix
	        name = name.Substring(0, name.Length - 10);

            // register router
	        if (String.IsNullOrEmpty(routeName)) routeName = name;

	        // check if this controller exists
	        // this will happen if the application tries to register the multiple controllers to the same route or simply adds the same controller multiple times
	        if (m_RegisteredControllers.ContainsKey(routeName)) return;

            // register views (contained in the controllers assembly)
            Controller.RegisterViews<T>();

            m_RegisteredControllers.Add(routeName, new RegisteredController()
	        {
	            ControllerType = typeof(T),
                AppContext = appContext,
                Name = name
            });

	        // the first controller is used as default controller
	        if (m_DefaultController == null)
	            m_DefaultController = m_RegisteredControllers[routeName];
	    }

        /// <summary>
        /// Sets a controller as default (the controller type has to be register first). 
        /// This one is used when calling 'Run' on the context during application start.
        /// By default, the first registered controller is made to the default controller.
        /// </summary>
        /// <typeparam name="T">type of controller to register</typeparam>
        public void SetDefaultController<T>() where T : Controller
        {
            // check if this controller is available
            foreach (var pair in m_RegisteredControllers)
            {
                if (pair.Value.ControllerType == typeof(T))
                {
                    Log.Info($"Controller '{pair.Value.Name}' is now used as Default Controller");
                    m_DefaultController = pair.Value;
                    return;
                }
            }

            // not registered!
            Log.Error($"SetDefaultController: Controller of type '{typeof(T).Name}' not registered.");
        }


        public bool HandleRequest(string url, bool addHistoryItem = true)
        {
			// If the URL is not our own custom scheme, just let the webView load the URL as usual
			var scheme = m_Context.UrlScheme;

			if (!url.StartsWith(scheme))
				return false;

			if (url == scheme)
				return false;

			// This handler will treat everything between the protocol and "?"
			// as the method name.  The query string has all of the parameters.
			var resources = url.Substring(scheme.Length).Split('?');
			var actionName = resources[0];
			var controllerName = m_DefaultController.Name;
			if (actionName.Contains ("/")) {
				var parts = actionName.Split ('/');
				controllerName = parts[0];
				actionName = parts[1];
			}

            var parameters = resources.Length > 1 ? ParseQueryString(resources[1]) : null;

            // check if this controller is available
            if (!m_RegisteredControllers.ContainsKey(controllerName))
            {
                Log.Error($"Controller '{controllerName}' not found.");
                return false;
            }

            // create a controller instance
            var controller = Controller.CreateController(m_Context, m_RegisteredControllers[controllerName]);

		    var methods = controller.GetType().GetRuntimeMethods();
		    foreach (var method in methods)
		    {
                // wrong name
		        if(method.Name != actionName) continue;

                var methodParams = method.GetParameters();

                // no arguments
		        if (methodParams.Length == 0 && parameters == null)
		        {
                    // do the navigation
                    Navigate(url, controller, method, addHistoryItem);
                    return true;
                }
                       
                // wrong argument count
                if (methodParams.Length != parameters.Count) continue;

                // right count
                var paramsIn = new object[methodParams.Length];

                // parse parameters (could be faster i guess)
                try
		        {
                    foreach (var p in methodParams)
                        paramsIn[Array.IndexOf(methodParams, p)] = parameters[p.Name] != null ?
                            Convert.ChangeType(parameters[p.Name], p.ParameterType) : null;
                }
		        catch (Exception)
		        {
		            continue;
		        }

                // do the navigation
                Navigate(url, controller, method, addHistoryItem, paramsIn);
                return true;
            }

            Log.Error($"Controller '{controllerName}' has no matching '{actionName}' Method found for: {url}" );
            return false;
        }

	    private void Navigate(string url, Controller controller, MethodInfo action, bool addHistoryItem, params object[] arguments)
	    {
	        NavigationHistoryItem itemToDispose = null;

            // add this call to history
            if(addHistoryItem)
                m_Context.History.AddHistoryItem(new MvcNavigationHistoryItem(this, url));
            else
                itemToDispose = m_Context.History.ReplaceCurrent(new MvcNavigationHistoryItem(this, url));

            // dispose the current controller
	        var oldController = ActiveController;

            // keep track of the current one
	        ActiveController = controller;

            // call the action
            action.Invoke(controller, arguments);

	        oldController?.Dispose();
            itemToDispose?.Dispose();

	        Log.Info($"Navigated to Controller View: {controller.Name} - {action.Name} (History Stack Size: {m_Context.History.Count})");
        }


        private Dictionary<string, string> ParseQueryString(HtmlString queryString)
        {
            string query = queryString;
            if (query[0] == '?') query = query.Substring(1); // remove leader query marker

            string[] keyValuePairs = query.Split(new char[] { '&' }, StringSplitOptions.RemoveEmptyEntries);
            var dict = new Dictionary<string, string>(keyValuePairs.Length);
            foreach (var pair in keyValuePairs)
            {
                int equalSignPos = pair.IndexOf('=');
                dict.Add(pair.Substring(0, equalSignPos), pair.Substring(equalSignPos + 1));
            }
            return dict;
        }
    }
}

