﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PortableRazor.Utilities;

namespace PortableRazor.Web.Mvc
{
    public partial class MvcContext
    {
        private List<MvcViewBase> m_ActiveMvcViews = new List<MvcViewBase>();

        private readonly PortableRazorContext m_Context;
        protected readonly Logging Log;

        internal MvcContext(PortableRazorContext context)
        {
            m_Context = context;
            Log = new Logging(this.GetType().Name);
        }

        /// <summary>
        /// Called by the Controller class to change the view.
        /// </summary>
        /// <param name="controller">the calling controller</param>
        /// <param name="viewName">the name of the view to navigate to</param>
        /// <param name="layoutName">the layout to use with the view (or null to use the default one)</param>
        /// <param name="viewbag">parameters to pass to the cshtml part of the view</param>
        internal void Navigate(Controller controller, string viewName, string layoutName, ControllerViewBag viewbag)
        {
            // check if the requested view and layout is available
            Type viewType = Controller.GetRegisteredView($"{controller.Name}.{viewName}");
            if (viewType == null) return /* 404 */;

            Type layoutType = null;
            if (!String.IsNullOrEmpty(layoutName))
            {
                // if there is a layout we use it (at least if we can find it
                layoutType = Controller.GetRegisteredView(layoutName);
                if (layoutType == null) return;  /* 404 */
            }

            // store old views for disposing
            List<MvcViewBase> oldViews = m_ActiveMvcViews;
            m_ActiveMvcViews = new List<MvcViewBase>();
        
            // create an instance of this view
            var view = CreateView(viewType, viewbag);

            // if there is no layout, we just render the page
            if (layoutType == null)
            {
                m_Context.WebView.LoadHtmlString(view.GenerateString());
            }
            else
            {
                // create the layout, pass the body and render them
                var layout = CreateLayout(layoutType, view);

                // not sure if this a the way to do it (maybe another model makes sense, too)
                m_Context.WebView.LoadHtmlString(layout.GenerateString());
            }

            // free old views
            foreach (var oldView in oldViews)
                oldView.Dispose();
            oldViews.Clear();

        }

        /// <summary>
        /// Used by the HtmlHelper to create partial views.
        /// </summary>
        /// <param name="partialViewName"></param>
        /// <param name="viewbag"></param>
        /// <returns></returns>
        internal MvcViewBase CreatePartial(string partialViewName, dynamic viewbag)
        {
            var partialType = Controller.GetRegisteredView(partialViewName);
            if (partialType == null)
            {
                Log.Error($"Partial view not found: {partialViewName}");
            }
            return CreateView(partialType, viewbag);
        }

        private MvcViewBase CreateView(Type viewType, dynamic viewbag)
        {
            var view = (MvcViewBase)Activator.CreateInstance(viewType);
            view.InitializeView(m_Context, viewbag);
            m_ActiveMvcViews.Add(view);
            return view;
        }

        private MvcLayoutBase CreateLayout(Type layoutType, MvcViewBase body)
        {
            var layout = (MvcLayoutBase)CreateView(layoutType, body.ViewBag);
            layout.InitializeLayout(body);
            return layout;
        }
    }
}
