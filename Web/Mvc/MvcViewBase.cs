using System;
using System.IO;
using PortableRazor.Web.Html;
using PortableRazor.Web.Mvc;

namespace PortableRazor
{
    public interface IViewBag
    {
        dynamic Model { get; set; }
    }

	//This was copied from the generated .cs for one of the razor views
	public abstract class MvcViewBase : RazorRenderable
    {
        private PortableRazorContext Context { get; set; }

        /// <summary>
        /// Can be accessed from cshtml files.
        /// </summary>
        public dynamic ViewBag { get; set; }

        /// <summary>
        /// Allows to go back in history. This will call the previews controller and action with the same parameters. 
        /// </summary>
        public NavigationHistory History => Context.History;

        protected HtmlHelper Html { get; private set; }
        protected MvcHelper Mvc { get; private set; }

        /// <summary>
        /// Interface class to allow Mvc bases Programs to use insert MVVM Elements.
        /// This allows mixture Models and offers flexibility.
        /// But the most important reason is that purely MVVM based applications can be hard to setup, especially if remote logins and so on are required.
        /// Therefore, we will usually use the Mvc pattern for app startup and higher level navigation and embedd MVVM to realize the more specific tasks.
        /// </summary>
        protected MvcToMvvmHelper MvvmNavigation { get; private set; }

	    /// <summary>
	    /// Allows to register custom content writer.
	    /// </summary>
	    /// <param name="context"></param>
	    /// <param name="viewbag"></param>
	    internal void InitializeView(PortableRazorContext context, dynamic viewbag)
        {
            Context = context;
            ViewBag = viewbag;
            Html = new HtmlHelper(Context, this);
            Mvc = new MvcHelper(Context, this);
            MvvmNavigation = new MvcToMvvmHelper(Context, this);
        }
        
	    public override void Dispose()
	    {
            Html.Dispose();
            Mvc.Dispose();
            MvvmNavigation.Dispose();
	        base.Dispose();
	    }

	    public override HtmlString RenderBindingScript()
	    {
            return Html.RenderBindingScript();
	    }
    }
}

