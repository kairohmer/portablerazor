﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PortableRazor.Json;
using PortableRazor.Web.Binding;
using PortableRazor.Web.Mvc;
using PortableRazor.Web.Mvvm;

namespace PortableRazor
{
    public class PortableRazorContext
    {
        /// <summary>
        /// The web view that this context is assigned to.
        /// Each web view has exactly one context.
        /// </summary>
        internal readonly IHybridWebView WebView;

        /// <summary>
        /// Allows to go back in history. This will call the previews controller and action with the same parameters. 
        /// </summary>
        internal readonly NavigationHistory History;


        public MvcContext Mvc { get; private set; }

        /// <summary>
        /// For navigation inside the app we use our own protocol. So links do not begin with "http:" but with this string.
        /// </summary>
        public string UrlScheme
        {
            get => m_Scheme ?? "razor:";
            set => m_Scheme = value;
        }
        private string m_Scheme;

        /// <summary>
        /// Creates a new context. This is done only by the native RazorWebView implementations.
        /// </summary>
        /// <param name="webView">the web view that created this context</param>
        public PortableRazorContext(IHybridWebView webView)
        {
            WebView = webView;

            // Process route events from the RazorWebview.
            WebView.RoutingRequest += (sender, args) => { args.Handled = Mvc.HandleRequest(args.Target.AbsoluteUri); };

            // register java handling
            WebView.NotifyFromJavascript += OnNotifyFromJavascriptAsync; // get notified about messages from js

            Mvc = new MvcContext(this);
            History = new NavigationHistory(this);
        }

        /// <summary>
        /// Starts the application by performing the first navigation to a action of the 'DefaultController' specified by the "Mvc". 
        /// This is usually called by the application and is done only once.
        /// </summary>
        /// <param name="action"></param>
        public void Run(string action)
        {
            if (m_IsRunning) return; // ignore all further calls

            Mvc.HandleRequest($"{UrlScheme}{action}");
            m_IsRunning = true;
        }
        private bool m_IsRunning = false; // make sure the run method is not misused


        /// <summary>
        /// Processes all messages from the js part.
        /// </summary>
        /// <param name="sender">the webView</param>
        /// <param name="message">most likely, a url envoded message of json object</param>
        private async void OnNotifyFromJavascriptAsync(object sender, string message)
        {
            if (message.StartsWith("razorcall:"))
            {
                string decoded = WebUtility.UrlDecode(message.Substring(10));
                dynamic deserialized = JsonConvert.DeserializeObject(decoded);

                string methodName = deserialized.Method.ToObject<String>();
                dynamic args = deserialized.Arguments.ToObject<dynamic>();

                var controller = Mvc.ActiveController;

                //var methods = controller.GetType().GetTypeInfo().Get(methodName); // not finding the correct methods
                var methods = controller.GetType().GetRuntimeMethods();
                foreach (var method in methods)
                {
                    // wrong name
                    if (method.Name != methodName) continue;

                    // no arguments
                    var methodParams = method.GetParameters();
                    if (methodParams.Length == 0 && args == null)
                    {
                        // invoke and return
                        method.Invoke(controller, new object[0]);
                        return;
                    }

                    // one parameter (only supported option)
                    if (methodParams.Length == 1)
                    {
                        try
                        {
                            method.Invoke(controller, new object[] {args});
                        }
                        catch (Exception)
                        {
                            // wrong parameters
                            // todo check types
                        }
                    }

                    // todo more arguments (but for now dynamic can handle everything... not very type safe)

                }
            }

            if (message.StartsWith("razorbindingevent:"))
            {
                string decoded = WebUtility.UrlDecode(message.Substring(18));
                dynamic deserialized = JsonConvert.DeserializeObject(decoded);

                if (deserialized == null)
                {
                    throw new Exception("Failed to deserialize: " + message);
                }

                string bindableGuidString = deserialized.handle.ToObject<string>();
                Guid bindableGuid;
                if (!Guid.TryParse(bindableGuidString, out bindableGuid))
                {
                    throw new Exception("Failed to pase GUID: " + bindableGuidString);
                }

                var bindable = RegisteredObject.GetByGuid<BindingId>(bindableGuid);
                bindable?.Invoke(deserialized.value.ToObject<string>());
            }

            if (message.StartsWith("razornavigateslot:"))
            {
                string decoded = WebUtility.UrlDecode(message.Substring(18));
                dynamic deserialized = JsonConvert.DeserializeObject(decoded);

                dynamic[] args = deserialized.ModelParamters.ToObject<dynamic[]>();
                Type[] argTypes = deserialized.ModelParamterTypes.ToObject<Type[]>();
                object[] values = args == null ? new object[0] : Utilities.InternalUtilities.ResolveArgumentValues(args, argTypes);

                var slot = RegisteredObject.GetByGuid<BindableViewModelSlot>(deserialized.SlotHandle.ToObject<Guid>());
                if (slot == null) return;

                Type viewModelType = deserialized.ViewModelType.ToObject<Type>();
                bool history = deserialized.History.ToObject<bool>();

                var token = new NavigationHelper.NavigationToken(slot.Navigation, slot, viewModelType)
                {
                    ModelParameters = values,
                    History = history
                };
                token.Go();
            }

            if (message.StartsWith("razornavigatebackslot:"))
            {
                await History.GoBackAsync();
            }
        }

       
        // -- experimental -- will be replaces by a plugin system or at least made a bit more general

        public void Register(IHardwareButtonListener listener)
        {
            listener.Back += OnHardwareButtonBack;
        }

        protected virtual void OnHardwareButtonBack(object sender, EventArgs eventArgs)
        {
            var task = History.GoBackAsync();
        }
    }
}
